﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Rhino.Mocks;
using UnityEngine;
using Object = UnityEngine.Object;


namespace MatchThree.CSharp.Test.Assets.Scripts.Match_3.Tiles
{
    [TestFixture]
    public class GemTests
    {
        [Test]
        public void check_something()
        {
            //Arrange
            var tile = new GameObject();
            GameObject blankTile0 = (GameObject)Object.Instantiate(tile, new Vector3(0, 0, 0), Quaternion.identity);

            Tile[,] tileMap = new Tile[4,4];
            tileMap[0,0] = blankTile0.GetComponent<Tile>();
            /*tileMap[0,1] = new Tile(0,1);
            tileMap[1,0] = new Tile(1,0);
            tileMap[1,1] = new Tile(1,1);*/

            var gem = new Gem(0, 0, 0, 0, tileMap);

            //Act
            var explodableGems = gem.getExplodableGems();

            //Assert
            LinkedList<Tile> expectedExplodableGems = new LinkedList<Tile>();

            Assert.AreEqual(expectedExplodableGems, explodableGems.Count);
        }
    }
}
