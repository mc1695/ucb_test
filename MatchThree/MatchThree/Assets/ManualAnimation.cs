﻿using UnityEngine;
using System.Collections;
using System;

public class ManualAnimation : MonoBehaviour {

    private int index = 0;
    [SerializeField]
    private Sprite[] images;
    [SerializeField]
    private float frameRateMultiplier = 1.0f;
    private float marker = 0.0f;
    private SpriteRenderer spriteRenderer;
    private float deltaTime = 0.0f;
    private float timeLastFrame = 0.0f;
    private bool paused = false;

	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        Array.Sort(images, (x, y) => String.Compare(x.name, y.name));
    }
	
	// Update is called once per frame
	void Update () {

        if (!paused)
        {
            marker += deltaTime * frameRateMultiplier;
            index = Mathf.RoundToInt(marker) % images.Length;
            spriteRenderer.sprite = images[index];
        }
        deltaTime = Time.realtimeSinceStartup - timeLastFrame;
        timeLastFrame = Time.realtimeSinceStartup;
    }

    public bool Paused
    {
        get
        {
            return paused;
        }set
        {
            paused = value;
        }
    }
}
