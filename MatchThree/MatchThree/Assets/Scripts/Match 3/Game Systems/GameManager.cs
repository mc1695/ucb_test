﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Diagnostics;
//using Fabric.Answers;

public class GameManager : MonoBehaviour
{
    //Board Data
    public Level currentLevel;
    Utilities utilities;
    int maxMovesAllowed;
    //UI
    int points;
    public Text pointsText;
    public int moves;
    public Text movesText;
    public Text maxMovesText;

    public UISystem uiSystem;
    ////Debug 
    //public mousePosTracker mouseTrack;

    public Utilities.Direction previousSwipeDirection;

    private bool gameOver;

    private bool downloadedLevel = false;
    private bool isEndScreenShowing = false;

    // Use this for initialization
    void Start ()
    {
        Application.targetFrameRate = 60;
        Time.timeScale = 1.0f;
        currentLevel.initialiseLevel();
        maxMovesAllowed = currentLevel.maxMoves;
        utilities = Utilities.getInstance();
        utilities.InitLineRender(this);
        currentLevel.isMovementLocked = false;
        moves = 0;

        uiSystem = GameObject.FindGameObjectWithTag(Tags.PointsUI).GetComponent<UISystem>();
        uiSystem.setMaxMoves(maxMovesAllowed);

        gameOver = false;
    }
	
	// Update is called once per frame
	void Update ()
	{
        try
        {
            if (!gameOver)
            {
                GetUserInput();
            }
            currentLevel.PreManualUpdate();
            switch (Rules.loseRequirementSelected)
            {
                case Level.loseRequirement.timer:
                    if (!uiSystem.isRemainingTime())
                    {
                        gameOver = true;
                        GameLost();
                    }
                    break;
                case Level.loseRequirement.noMovesLeft:
                    if (moves >= maxMovesAllowed)
                    {
                        gameOver = true;
                        GameLost();
                    }
                    break;
                default:
                    break;
            }
        }catch(Exception ex)
        {
            // Get stack trace for the exception with source file information
            StackTrace stackTrace = new StackTrace(ex, true);
            // Get the top stack frame and
            // Get the line number from the stack frame
            int line = stackTrace.GetFrame(0).GetFileLineNumber();

            GameAnalytics.LogError("GameManager Caught Exception", "Exception caught in GameManager Update()", ex.StackTrace, SceneManager.GetActiveScene().name, "Line: " + line.ToString());
            Utilities.DebugMsg(ex.Message + " "+ ex.Source + " " + ex.StackTrace);
        }
	}

    void LateUpdate()
    {
        currentLevel.ManualUpdate();
        currentLevel.PostUpdate();
    }
    

    public void UpdatePoints(int gemPoints)
    {
        points += gemPoints;
        uiSystem.IncreaseScore(points);
        uiSystem.IncreaseTimer(gemPoints / 60);
    }

    void GameWon()
    {

    }

    public void GameLost()
    {
        if(currentLevel.gemsAnimating())
        {
            uiSystem.setTimer(0.0f);
            return;
        }
        if (!isEndScreenShowing)
        {
            int turnsRemaining = PlayerPrefs.GetInt(SaveManager.remainingEnergyCount);
            GameAnalytics.PlayedAGame(turnsRemaining, Time.timeSinceLevelLoad, points, moves, downloadedLevel, currentLevel.LongestTimeBetweenMoves);
            GameAnalytics.PlayerModelData(currentLevel.AverageTimeBetweenMoves, currentLevel.InvalidMovesMade, currentLevel.MovesMade);
            //currentLevel.isMovementLocked = true;
            Time.timeScale = 0;
            uiSystem.showGameOverScreen(points);
            string[] highScore = PlayerPrefs.GetString(SaveManager.dailyHighScore).Split('|');
            if (int.Parse(highScore[0]) < points)
            {
                PlayerPrefs.SetString(SaveManager.dailyHighScore, SaveManager.pointsToScoreData(points));
                uiSystem.showHighScoreMessage();
            }
            isEndScreenShowing = true;
        }
    }

    /// <summary>
    /// Locate the Gem Selected and the Gem to attempt a swap with
    /// </summary>
    void GetUserInput()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0) && !gameOver) 
        {
            //Debug.Log("Mouse Down");
            if (!currentLevel.isMovementLocked)
            {
                
                Utilities.Direction swipeDir = utilities.getSwipeDirection();
                //get current position // get swapping gem // make swap
                //Vector2 gemPosition = Input.mousePosition;
                //mouseTrack.swipeDir = swipeDir;
                if (swipeDir != Utilities.Direction.None)
                {
                    //mouseTrack.swipeDir = swipeDir;
                    Vector3 screenPoint = new Vector3(utilities.startTouch.x, utilities.startTouch.y);
                    screenPoint = Camera.main.ScreenToWorldPoint(screenPoint);

                    Vector2 gemPos = new Vector2(screenPoint.x, screenPoint.y);
                    gemPos = checkBounds(gemPos);

                    Tile startGem;
                    try
                    {
                        startGem = currentLevel.getTile(gemPos, true);
                    }catch(IndexOutOfRangeException ex)
                    {
                        Utilities.DebugMsg("User tried to drag from invalid location: " + ex.Message);
                        return;
                    }
                    

                    Vector2 swapGemPos = gemPos;
                    switch (swipeDir)
                    {
                        case Utilities.Direction.Up:
                            swapGemPos.y += 1;
                            break;
                        case Utilities.Direction.Right:
                            swapGemPos.x += 1;
                            break;
                        case Utilities.Direction.Down:
                            swapGemPos.y -= 1;
                            break;
                        case Utilities.Direction.Left:
                            swapGemPos.x -= 1;
                            break;
                        case Utilities.Direction.None:
                            return;
                        default:
                            return;
                    }
                    //Debug.Log("Swap Gem: ");
                    swapGemPos = checkBounds(swapGemPos);

                    Tile swapGem;
                    try
                    {
                        swapGem = currentLevel.getTile(swapGemPos);
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        Utilities.DebugMsg("User tried to drag to invalid location: " + ex.Message);
                        return;
                    }


                    if(Utilities.isInvalidTile(swapGem.id) || Utilities.isInvalidTile(startGem.id))
                    {
                        utilities.mouseUp();
                        return;
                    }
                    
                    currentLevel.addToSwapQueue(startGem, swapGem);

                    previousSwipeDirection = swipeDir;
                    
                    //Queue Swap in Current Level
                }
            }
        }
        if(Input.GetMouseButton(0))
        {
            utilities.swipeIndication();
        }
        if(Input.GetMouseButtonUp(0))
        {
            utilities.mouseUp();
        }
        
    }

    /// <summary>
    /// Ensures the mouse position is within the game bounds and rounds it to the 
    /// neares int to attempt to relate a tile to the mouse position
    /// </summary>
    /// <returns>A Vector that should correspond to a tile with currentLevel.map</returns>
    Vector2 checkBounds(Vector2 position)
    {
        Vector2 mapSize = currentLevel.mapSize;

        position.x = Mathf.RoundToInt(position.x);
        position.y = Mathf.RoundToInt(position.y);
        //Debug.Log("Click Pos: " + position.ToString());
        if (position.x > mapSize.x)
        {
            position.x = mapSize.x * currentLevel.tileSize;
        }
        if(position.x < 0)
        {
            position.x = 0;
        }
        if(position.y > mapSize.y)
        {
            position.y = mapSize.y * currentLevel.tileSize;
        }
        if(position.y < 0)
        {
            position.y = 0;
        }
        //Debug.Log("Gem Pos: " + position.ToString());
        return position;
    }

    public bool DownloadedLevel
    {
        set
        {
            downloadedLevel = value;
        }
    }

    public int Points
    {
        get
        {
            return points;
        }
    }

}
