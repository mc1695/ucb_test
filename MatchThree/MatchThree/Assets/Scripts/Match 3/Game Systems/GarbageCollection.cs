﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class GarbageCollection : MonoBehaviour
{
    Transform Garbage;
    Utilities utilities;
	// Use this for initialization
	void Start ()
    {
        Garbage = this.transform;
        utilities = Utilities.getInstance();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
	    if(Garbage.childCount != 0)
        {
            Tile currentTile = Garbage.GetChild(0).GetComponent<Tile>();
            if(currentTile != null && (!currentTile.isAnimating || currentTile.id == -1))
            {
                if (!Utilities.isInvalidTile(currentTile.id))
                {
                    checkSurroundingTiles(currentTile);
                }

                DestroyImmediate(currentTile.gameObject);
                DestroyImmediate(currentTile);
            }
        }
	}

    public void addToGarbage(Tile rubbishTile)
    {
        if (rubbishTile != null)
        {
            if(rubbishTile.GetComponent<Collider2D>() != null)
            {
                Destroy(rubbishTile.GetComponent<Collider2D>());
            }
            rubbishTile.transform.parent = Garbage;
            if (rubbishTile.getAudioClip() != null)
            {
                GameObject.FindGameObjectWithTag(Tags.AudioPlayer).GetComponent<AudioManager>().PlayExplosion(rubbishTile.getAudioClip());
            }
            utilities.CreateExplosion(rubbishTile.id, rubbishTile.transform.position);
        }
    }

    //TODO: Check surronding tiles for any knock on effects
    void checkSurroundingTiles(Tile currentTile)
    {
        //checks the type of all 4 neighbours to see if they need their health lowering
        //i.e. lock gems and blocking gems
        Level currentLevel = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();

        //Tile currentTile = currentLevel.getTile(position, true);

        if (currentTile == null)
        {
            return;
        }
        if (Utilities.isInvalidTile(currentTile.id))
        {
            return;
        }


        Vector2 neighbourPosition = currentTile.Position;
        //Up
        neighbourPosition.y += 1;
        if (currentTile.isInBounds(Utilities.Direction.None, 0))
            currentLevel.getTile(currentTile.Position).neighbourDestroyed();
        neighbourPosition.y -= 1;
        //
        //Right
        neighbourPosition.x += 1;
        if (currentTile.isInBounds(Utilities.Direction.None, 0))
            currentLevel.getTile(currentTile.Position).neighbourDestroyed();
        neighbourPosition.x -= 1;
        //
        //Down
        neighbourPosition.y -= 1;
        if (currentTile.isInBounds(Utilities.Direction.None, 0))
            currentLevel.getTile(currentTile.Position).neighbourDestroyed();
        neighbourPosition.y += 1;
        //
        //Left
        neighbourPosition.x -= 1;
        if (currentTile.isInBounds(Utilities.Direction.None, 0))
            currentLevel.getTile(currentTile.Position).neighbourDestroyed();
        neighbourPosition.x += 1;
    }
}
