﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    //Rules
    #region Rules
    public enum winRequirement
    {
        collectGems,
        playUntilLose
    }
    public winRequirement winRequirementSelected = winRequirement.collectGems;
    //public bool continuePlayingAfterWin;


    public enum loseRequirement
    {
        timer,
        noMovesLeft
    }
    public loseRequirement loseRequirementSelected = loseRequirement.timer;

    //manage turns
    public int maxMoves;
    public int movesRemaining;
    //public int movesGainedForExplodingSameColorInTwoAdjacentTurn;
    //public int[] movesGainedWhenExplodingMoreThan3Gems;

    public bool gameEnd;
    
    public enum destroyedTileReward
    {
        nothing,
        moreTime,
        moreMoves
    }
    public destroyedTileReward tile_give = destroyedTileReward.nothing;
    public float timeToAdd; //how much time give
    //public float time_bonus_for_secondary_explosion;
    //public float time_bonus_for_gem_explosion;
    
    public float timer;
    public float timeLeft;
    public float timeBonus;
    public bool stageStarted;
    public float startTime;

    //keep note of the gems destryed
    public int[] numberOfGemsToDestroy;

    public int[] totalNumberOfGemsDestroyed;
    //public int[] number_of_gems_collect_by_the_player;
    //bool[] player_this_gem_color_is_collected;
    //public int total_number_of_gems_remaining_for_the_player;
    //public int total_number_of_gems_required_colletted;

    //bonus

    //Combo
    public bool comboGemsOn;
    #endregion
    //Level Properties
    [HideInInspector]
    public Vector2 mapSize;
    public bool useLocalLevelFile;
    public TextAsset levelFile;
    public Tile[,] map;
    public float tileSize;

    //Utillity functions
    Utilities utilities;
    public GarbageCollection garbage;
    public SpawnManager spawnManager;
    public bool isMovementLocked;

    //UI
    UISystem UI;

    //Map 
    public int[,,] board_array_master;
    public List<LinkedList<Tile>> matches;
    private bool isPreviousMatchChecked;
    private Queue<Vector2> swapQueue;

    //DebugToUi
    public PretendConsole console;

    //Gem Shine
    [Range(0,15)]
    public int timeToWaitBetweenShines;
    public bool shineAllSameColourGems;
    [Range(0, 10)]
    public int amountOfGemsToShine;
    float timeSinceLastShine;
    
    private float longestNonMoveTime = 0;
    private float timeAtLastMove;
    private bool successfulMove = false;
    private int failedMoves = 0;
    private bool registeredFailedMove = true;
    private int movesMade = 0;

    //--Tutorial mode settings--
    [SerializeField]
    bool tutorialMode = false;
    [SerializeField]
    GameObject playButton;
    // ----------------------

    private int successfulMoves = 0;

    // Use this for initialization
    void Start ()
    {
        utilities = Utilities.getInstance();
        //isMovementLocked = false;
        matches = new List<LinkedList<Tile>>();

        if (spawnManager == null)
        {
            spawnManager = GameObject.FindGameObjectWithTag(Tags.Spawn).GetComponent<SpawnManager>();
        }
        if (garbage == null)
        {
            garbage = GameObject.FindGameObjectWithTag(Tags.Garbage).GetComponent<GarbageCollection>();
        }

        isPreviousMatchChecked = true;
        swapQueue = new Queue<Vector2>();

        timeSinceLastShine = 0;
    }


    public void PreManualUpdate()
    {

        for (int x = (int)mapSize.x -1; x>= 0; x--)
        {
            for(int y = (int)mapSize.y-1; y>=0; y--)
            {
                map[x, y].ManualUpdate();
            }
        }

        foreach (Transform child in spawnManager.transform)
        {
            Tile currentTile = child.GetComponent<Tile>();
            if (getTile(currentTile.Position, true) != currentTile)
            {
                currentTile.id = -1;
                currentTile.name = "PROACTIVE (SILLY) TILE";
                //Destroy(currentTile);
            }
        }
    }

	// Update is called once per frame
	public void ManualUpdate ()
    {
        if (successfulMove)
        {
            successfulMoves++;
            movesMade++;
            successfulMove = false;
            updateLongestMoveTime();
        }
        if(!registeredFailedMove)
        {
            failedMoves++;
            movesMade++;
            registeredFailedMove = true;
        }
        
        //tilesMoving?
        for (int x = 0; x < mapSize.x; x++)
        {
            for (int y = 0; y < mapSize.y; y++)
            {
                if (map[x, y] != null && (map[x, y].isAnimating || map[x, y].getAnimationCount() > 0))
                {
                    //Debug.Log("Tile at (" + x + "," + y + ") isAnimating = " + map[x, y].isAnimating + " and queue len = " + map[x, y].animationQueue.Count);
                    isPreviousMatchChecked = false;
                    return;
                }
            }
        }

        //prevMatchChecked?
        if (!isPreviousMatchChecked)
        {
            postSwapChecks();
            isPreviousMatchChecked = true;
            //remove match3s
            if (matches.Count > 0)
            {
                LinkedList<Tile> currentMatch = matches[0];
                if (currentMatch != null)
                {
                    //THIS IS TOO BIG A CHANGE TO MAKE RIGHT NOW..
                    //FOREACH LOOP NEEDS DESTROYING/REWRITING/FUCK KNOWS
                    foreach (Tile currentTile in currentMatch)
                    {
                        currentMatch.First.Value.removeMatch3(currentMatch);
                        if (matches.Count > 0)
                        {
                            if (matches[0] == null)
                            {
                                isPreviousMatchChecked = false;
                                return;
                            }
                            matches.RemoveAt(0);
                        }
                    }
                }
                else
                {
                    matches.RemoveAt(0);
                }

                isPreviousMatchChecked = false;
                return;
            }
            return;
        }
        //swaps > 1 ?
	    if (swapQueue.Count > 1)
	    {
            //if swap still valid
	        Utilities.Direction dir;
            Tile firstTile = getTile(swapQueue.Dequeue());
            Tile swapTile = getTile(swapQueue.Dequeue());
            if (isValidSwap(firstTile, swapTile, out dir))
	        {
	            if (firstTile.swap(swapTile, dir, true))
	            {
	                GameObject.FindGameObjectWithTag(Tags.World).GetComponent<GameManager>().moves++;
                    UI.IncrementMoves();
	                isPreviousMatchChecked = false;

                    successfulMove = true;
                }
                else
                {
                    registeredFailedMove = false;
                }
	        }
            return;
	    }
        
        if (tutorialMode)
        {
            if (successfulMoves > 0)
            {
                GameManager gameManager = GameObject.FindObjectOfType<GameManager>();
                while (true)
                {
                    gameManager.GameLost();
                    playButton.SetActive(true);
                    this.GetComponent<LineRenderer>().material.SetColor("_Color", new Color(1, 1, 1, 0));
                    return;
                }
            }
        }

        //moveAvailable?
        if (!isMoveAvailable())
	    {
	        Shuffle();
	    }

        
        if (tutorialMode && !tutorialSwapping)
        {
            StartCoroutine(tutorialSwap());
        }

    }

    private void updateLongestMoveTime()
    {
        if (longestNonMoveTime < Time.timeSinceLevelLoad - timeAtLastMove)
        {
            longestNonMoveTime = Time.timeSinceLevelLoad - timeAtLastMove;
        }
        timeAtLastMove = Time.timeSinceLevelLoad;
    }

    public void PostUpdate()
    {
        spawnManager.ManualUpdate();
        timeSinceLastShine += Time.deltaTime;
        if(timeSinceLastShine<timeToWaitBetweenShines)
        {
            return;
        }
        if (shineAllSameColourGems)
        {
            int tileID = UnityEngine.Random.Range(1, 7);
            for (int x = (int)mapSize.x - 1; x >= 0; x--)
            {
                for (int y = (int)mapSize.y - 1; y >= 0; y--)
                {
                    if (map[x, y].id == tileID)
                    {
                        map[x, y].triggerShine();
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < amountOfGemsToShine; i++)
            {
                int tileID = -1;
                Tile currentTile = getTile(new Vector2(0,0));
                while (Utilities.isInvalidTile(tileID))
                {
                    currentTile = getTile(generateRandomCoords());
                    tileID = currentTile.id;
                }
                currentTile.triggerShine();
            }
        }
        timeSinceLastShine = 0;
    }

    Vector2 generateRandomCoords()
    {
        int x = (int)UnityEngine.Random.Range(0, mapSize.x);
        int y = (int)UnityEngine.Random.Range(0, mapSize.y);
        return new Vector2(x,y);
    }

    //change to above Tile[,] data struct
    public void initialiseLevel()
    {
        string fileContents = string.Empty;
        if (!useLocalLevelFile)
        {
            LevelGetter lvlGet = new LevelGetter("games@jarmacoe.com", "GenGame2016!");        
            try
            {
                lvlGet.checkForNewFiles();
                fileContents = lvlGet.getLevelRandom();
            }
            catch(Exception Ex)
            {
                GameAnalytics.LogError("Level load failure", "Exception caught in Level when trying to load level file", Ex.StackTrace, SceneManager.GetActiveScene().name, "File contents: " + fileContents);
                Utilities.DebugMsg("Failed to update level files from remote: "+Ex.Message);
            }
            //lvlGet.deleteAllLocalLevelData();
            //fileContents = lvlGet.getLevelByName("3match_specialTest.txt"); 
            
        }
        else
        {
            fileContents = levelFile.text;
        }
        Utilities.DebugMsg("file: " + fileContents);
        if(fileContents == null || fileContents == "")
        {
            Utilities.DebugMsg("File Empty");
        }
        //read in level file and create gems
        //create fileCode array
        try
        {
            string[] parts = fileContents.Split(new string[] { "\n" }, 0);
            mapSize.x = Int32.Parse(parts[0]);
            mapSize.y = Int32.Parse(parts[1]);
            
            //split map file
            map = new Tile[(int)mapSize.x, (int)mapSize.y];
            board_array_master = new int[(int)mapSize.x, (int)mapSize.y, 15];
            int storageX = (int)mapSize.x - 1;
            for (int x = 0; x < (int)mapSize.x; x++)
            {
                int storageY = (int)mapSize.y - 1;
                for (int y = 0; y < (int)mapSize.y + 2; y++)
                {
                    if (y > 1)
                    {
                        string[] tile = parts[y].Split(new string[] { "|" }, StringSplitOptions.None);

                        for (int z = 0; z < 5; z++)
                        {
                            string[] tileCharacteristic = tile[x].Split(new string[] { "," }, StringSplitOptions.None);
                            board_array_master[x, storageY, z] = Int16.Parse(tileCharacteristic[z]);
                        }
                        storageY--;
                    }
                }
                storageX--;
            }

            //create gems
            int mapHeight = (int)mapSize.y + 2;
            for (int x = 0; x < mapSize.x; x++)
            {
                for (int y = 0; y < mapSize.y; y++)
                {
                    //Debug.Log("Lvl info: (" + x + "," + y + "). \r\n0:"
                    //    + board_array_master[x, y, 0] + " 1: "
                    //   + board_array_master[x, y, 1] + ", 2:" + board_array_master[x, y, 2] + ", 3:" + board_array_master[x, y, 3]
                    //    + ", 4:" + board_array_master[x, y, 4]);

                    if (board_array_master[x, y, 4] != 0)
                    {
                        map[x, y] = utilities.CreateGem(x, y, board_array_master[x, y, 1], 1);///Create Normal Gem ready for upgrade
                        spawnManager.createComboGem(map[x, y], board_array_master[x, y, 4]);///upgrade gem
                    }
                    else if (board_array_master[x, y, 3] != 0)
                        map[x, y] = utilities.CreateGem(x, y, board_array_master[x, y, 3], 3);///Create Restraint
                    else if (board_array_master[x, y, 2] != 0)
                        map[x, y] = utilities.CreateGem(x, y, board_array_master[x, y, 2], 2);///Create Special Tile
                    else if (board_array_master[x, y, 0] != 0)
                        map[x, y] = utilities.CreateGem(x, y, board_array_master[x, y, 1], 1);///Create Normal Gem

                    
                    map[x, y].transform.parent = spawnManager.transform;
                }
            }
        }
        catch (Exception ex)
        {
            Utilities.DebugMsg("Level load fail: " + ex.StackTrace);
            GameAnalytics.LogError("Failed to load level", "Exception when parsing the level", ex.StackTrace, SceneManager.GetActiveScene().name, "File contents: " + fileContents);
            SceneManager.LoadScene(Scenes.Menu);
        }
     
        initialiseCanvas();
        spawnManager.initMap(map, mapSize);

        Rules.currentWinRequirement = winRequirementSelected;
        Rules.loseRequirementSelected = loseRequirementSelected;
        Rules.maxMoves = maxMoves;
        Rules.comboGemsOn = comboGemsOn;
    }

    bool tutorialSwapping = false;
    IEnumerator tutorialSwap()
    {
        tutorialSwapping = true;
        while(Time.timeSinceLevelLoad < 1.5f)
        {
            yield return null;
        }
        swapQueue.Enqueue(new Vector2(1, 0));
        swapQueue.Enqueue(new Vector2(1, 1));
        successfulMoves++;
    }

    public void initialiseCanvas()
    {
        UI = GameObject.FindGameObjectWithTag(Tags.PointsUI).GetComponent<UISystem>();
        UI.SetupCanvas(loseRequirementSelected);
    }
    
    public bool gemsAnimating()
    {
        if (spawnManager.gemsAnimating())
        {
            return true;
        }
        for (int x = (int)mapSize.x - 1; x >= 0; x--)
        {
            for (int y = (int)mapSize.y - 1; y >= 0; y--)
            {
                if(map[x, y].isAnimating || map[x,y].getAnimationCount() > 0)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void addToSwapQueue(Tile startTile, Tile swapTile)
    {
        if(swapQueue.Count > 0)
            return;
        if (!swapQueue.Contains(startTile.Position) && !swapQueue.Contains(swapTile.Position))
        {
            swapQueue.Enqueue(startTile.Position);
            swapQueue.Enqueue(swapTile.Position);
        }
    }

    private bool isValidSwap(Tile firstTile, Tile swapTile, out Utilities.Direction dir)
    {

        if(firstTile.transform.parent != spawnManager.transform || swapTile.transform.parent != spawnManager.transform)
        {
            dir = Utilities.Direction.None;
            return false;
        }
        if (firstTile.Position.x - swapTile.Position.x == 1 && firstTile.Position.y == swapTile.Position.y)
        {
            dir = Utilities.Direction.Left;
            return true;
        }
        else if(swapTile.Position.x - firstTile.Position.x == 1 && firstTile.Position.y == swapTile.Position.y)
        {
            dir = Utilities.Direction.Right;
            return true;
        }
        else if (firstTile.Position.y - swapTile.Position.y == 1 && firstTile.Position.x == swapTile.Position.x)
        {
            dir = Utilities.Direction.Down;
            return true;
        }
        else if (swapTile.Position.y - firstTile.Position.y == 1 && firstTile.Position.x == swapTile.Position.x)
        {
            dir = Utilities.Direction.Up;
            return true;
        }
        else
        {
            dir = Utilities.Direction.None;
            return false;
        }
    }

    public void Shuffle(int swapsMade = 0)
    {
        //If any tile is animating, don't shuffle
        for (int x = 0; x < mapSize.x; x++)
        {
            for (int y = 0; y < mapSize.y; y++)
            {
                if (map[x,y].isAnimating || map[x,y].getAnimationCount() > 0)
                {
                    return;
                }
            }
        }

        for (int x = 0; x < mapSize.x; x++)
        {
            for(int y = 0; y < mapSize.y; y++)
            {
                Tile tileA = getTile(new Vector2(x, y));
                if (tileA.id > 0 && tileA.id <= 6)
                {
                    int randX, randY;
                    randX = UnityEngine.Random.Range(0, (int)mapSize.x);
                    randY = UnityEngine.Random.Range(0, (int)mapSize.y);

                    Tile tileB = getTile(new Vector2(randX, randY));
                    if (tileB.id > 0 && tileB.id <= 6)
                    {
                        //do swap //if not a match? 
                        swapTiles(tileA, tileB);
                        if (!tileA.isMatchThree(tileA.id) && !tileB.isMatchThree(tileB.id))
                        {
                            swapsMade++;
                        }
                        else
                        {
                            swapTiles(tileA, tileB);
                        }
                    }
                }
            }
        }
        if(swapsMade < 3)
        {
            Shuffle(swapsMade);
        }
        else
        { 
            //animate all gems with shuffle animation(s)
            for (int x = 0; x < mapSize.x; x++)
            {
                for (int y = 0; y < mapSize.y; y++)
                {
                    if (!Utilities.isInvalidTile(map[x, y].id))
                    {
                        map[x, y].addAnimationTrigger("ShuffleShrink");
                        map[x, y].addAnimationTrigger("ShuffleGrow");
                    }
                }
            }
            return;
        }
    }

    public void swapTiles(Tile selectedTile, Tile swapTile)
    {
        // swap a + b
        // t = a
        // a = b
        // b = t

        
        Vector2 aPos = selectedTile.Position;
        Vector2 bPos = swapTile.Position;

        //if seleceted tile is outside the map then this is a new gem and the swap tile should be removed (it should be empty)
        if (selectedTile.Position.y >= mapSize.y || selectedTile.Position.y < 0)
        {
            map[(int)bPos.x, (int)bPos.y] = selectedTile;
            map[(int)bPos.x, (int)bPos.y].Position = bPos;
            
            garbage.addToGarbage(swapTile);
        }
        else
        {
            Tile holdingTile = map[(int)aPos.x, (int)aPos.y];

            //swap tiles
            //a
            map[(int)aPos.x, (int)aPos.y] = map[(int)bPos.x, (int)bPos.y];
            map[(int)aPos.x, (int)aPos.y].Position = aPos;
            //b
            map[(int)bPos.x, (int)bPos.y] = holdingTile;
            map[(int)bPos.x, (int)bPos.y].Position = bPos;
            
        }
    }


    public void postSwapChecks()
    {
        ////drop all tiles down, starting from bottom right
        //for (int x = (int)mapSize.x - 1; x >= 0; x--)
        //{
        //    for (int y = (int)mapSize.y - 1; y >= 0; y--)
        //    {
        //        map[x, y].isBelowTileEmpty();
        //    }
        //}

        spawnManager.CheckSpawnList(map, mapSize);
        HashSet<Vector2> explodablePositions = new HashSet<Vector2>();
        for (int x = 0; x < mapSize.x; x++)
        {
            for (int y = 0; y < mapSize.y; y++)
            {
                if (!explodablePositions.Contains(new Vector2(x, y)))
                {
                    if (map[x, y].isMatchThree(map[x, y].id))
                    {
                        //Utilities.DebugMsg("-- Adding tile (and explGems) to matches " + map[x, y].position + " --");
                        LinkedList<Tile> currentExplodables = map[x, y].getExplodableGems();

                        LinkedListNode<Tile> currentNode = currentExplodables.First;

                        for (int count = 0; count < currentExplodables.Count; count++)
                        {
                            if (!explodablePositions.Contains(currentNode.Value.Position))
                            {
                                explodablePositions.Add(currentNode.Value.Position);
                            }
                            currentNode = currentNode.Next;
                        }

                        matches.Add(map[x, y].getExplodableGems());
                    }
                }
            }
        }
    }

    public Tile destroyTile(Tile currentTile)
    {
        //points text anim
        if (currentTile.id >= 0)
        {
            this.GetComponent<GameManager>().UpdatePoints(currentTile.points);
            GameObject pointsTextPrefab = Resources.Load("Prefabs/UI/PointsText") as GameObject;
            Vector3 worldPos = new Vector3(currentTile.Position.x, currentTile.Position.y, 0);
            Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);

            GameObject pointsText = (GameObject)Instantiate(pointsTextPrefab, screenPos, Quaternion.identity);
            pointsText.GetComponent<Text>().text = "+" + currentTile.points;
            Transform canvas = GameObject.FindGameObjectWithTag(Tags.PointsUI).transform;
            pointsText.transform.SetParent(canvas);
            pointsText.GetComponent<Animator>().SetTrigger("Grow");
        }
        //
        //save tilePos
        Vector2 pos = currentTile.Position;
        //replace tile with an empty tile
        map[(int)pos.x, (int)pos.y] = utilities.CreateGem((int)pos.x, (int)pos.y, -1);
        map[(int) pos.x, (int) pos.y].transform.parent = spawnManager.transform;
        //delete the tile
        garbage.addToGarbage(currentTile);
        //return the empty tile
        return map[(int)pos.x, (int)pos.y];
    }

    public void dropTiles()
    {
        for (int x = 0; x < mapSize.x; x++)
        {
            for (int y = 0; y < mapSize.y; y++)
            {
                map[x, y].isBelowTileEmpty();
            }
        }
    }

    public Tile getTile(Vector2 pos, bool isBoundsTest = false)
    {
        if (!isBoundsTest)
        {
            return map[(int)pos.x, (int)pos.y];
        }
        else
        {
            if (pos.x >= 0 && pos.y >= 0 &&
                pos.x < mapSize.x && pos.y < mapSize.y)
                return map[(int)pos.x, (int)pos.y];
            else
                return null;
        }
    }

    public void setTile(Tile newTile)
    {
        //check positions currentTile
        if(map[(int)newTile.Position.x, (int)newTile.Position.y] != null)
        {
            Tile tileToDelete = map[(int)newTile.Position.x, (int)newTile.Position.y];
            map[(int)newTile.Position.x, (int)newTile.Position.y] = newTile;
            garbage.addToGarbage(tileToDelete);
        }
        else
        {
            map[(int)newTile.Position.x, (int)newTile.Position.y] = newTile;
        }
        
    }

    public void upgradeTile(Tile newTile)
    {
        map[(int)newTile.Position.x, (int)newTile.Position.y] = newTile;
    }

    public bool isMoveAvailable()
    {
        //isMovementLocked = true;
        for (int y = 0; y < mapSize.y; y++)
        {
            for (int x = 0; x < mapSize.x; x++)
            {
                if (map[x, y].isMovePossible(map, mapSize))
                {
                    //isMovementLocked = false;
                    return true;
                }
            }
        }
        //Debug.Log("NEED TO SHUFFLE");
        //isMovementLocked = false;
        return false;
    }

    void OnApplicationQuit()
    {
        GameAnalytics.QuitGame(PlayerPrefs.GetInt(SaveManager.energyCountAtLaunch,0) - PlayerPrefs.GetInt(SaveManager.remainingEnergyCount,1), Time.time, PlayerPrefs.GetInt(SaveManager.dailyHighScore));
    }

    public float LongestTimeBetweenMoves
    {
        get
        {
            return longestNonMoveTime;
        }
    }

    public float AverageTimeBetweenMoves
    {
        get
        {
            return (float)successfulMoves/Time.timeSinceLevelLoad;
        }
    }

    public int InvalidMovesMade
    {
        get
        {
            return failedMoves;
        }
    }

    public int MovesMade
    {
        get
        {
            return movesMade;
        }
    }
}