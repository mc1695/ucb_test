﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class Menu : MonoBehaviour
{
    public Text remainingEnergyValue;
    public Button playButton;
    public Text countdownToEnergyValue;

    [SerializeField]
    private bool forceEnergy = false;
    [SerializeField]
    private int forcedEnergyValue = 15;

    private int energy;
    private long timeLastSaved;

    [SerializeField]
    private long secondsPerEnergy = 5760;
    private static int energyMax = 15;

    private int startEnergy;

    public AnalyticsMode analyticsMode = AnalyticsMode.DEV;

    // Use this for initialization
    void Start()
    {
        GameAnalytics.getInstance().setUp(analyticsMode, -1);

        if (!PlayerPrefs.HasKey(SaveManager.previousEnergyUpdateTime)) //first time opening
        {
            energy = 15;
            SaveData(true);
        }

        if (!PlayerPrefs.HasKey(SaveManager.dailyHighScore))
        {
            PlayerPrefs.SetString(SaveManager.dailyHighScore, SaveManager.pointsToScoreData(0));
        }
        else
        {
            string[] data = PlayerPrefs.GetString(SaveManager.dailyHighScore).Split('|');
            DateTime storedDateTime = Convert.ToDateTime(data[1]);
            DateTime previousMidnight = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 1);
            if (storedDateTime < previousMidnight)
            {
                PlayerPrefs.SetString(SaveManager.dailyHighScore, SaveManager.pointsToScoreData(0));
            }
        }

        //
        //
        //TEST
        LoadData();

        float timeSinceLastUpdate = Utilities.UnixTimeNow() - timeLastSaved;
        if (Math.Abs(timeSinceLastUpdate) > secondsPerEnergy)
        {
            int energyGained = (int)(timeSinceLastUpdate / secondsPerEnergy)-1;
            energy += energyGained;
            if (energy > energyMax)
            {
                energy = energyMax;
            }
            SaveData(true);
        }

        //For debugging
        if (forceEnergy)
        {
            energy = forcedEnergyValue;
        }

        remainingEnergyValue.text = energy.ToString();
        GameAnalytics.OpenedApp(energy);
        Time.timeScale = 1.0f;

        startEnergy = 0;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateEnergyOverTime();

        if(energy-3 >= 0)
        {
            playButton.enabled = true;
            playButton.GetComponent<Image>().color = Color.white;
        }

        remainingEnergyValue.text = energy.ToString();
    }
    
    void UpdateEnergyOverTime()
    {
        //86400 sec/day, /15 = 5,760 seconds per energy
        if(energy < energyMax)
        {
            long now = (long)Utilities.UnixTimeNow();
            float timeSinceLastUpdate = now - timeLastSaved;
            bool save = false;

            int minutesRemaining = (int)((secondsPerEnergy - timeSinceLastUpdate) / 60);
            int secondsRemaining = (int)((secondsPerEnergy - timeSinceLastUpdate) % 60);

            //This works whilst the menu is open, but not otherwise
            if (minutesRemaining <= 0 && secondsRemaining <= 0)
            {
                energy++;
                save = true;
            }

            if (save)
            {
                timeLastSaved = Utilities.UnixTimeNow();
                SaveData(true);
            }
            remainingEnergyValue.text = energy.ToString();
            countdownToEnergyValue.text = minutesRemaining + " minutes, " + secondsRemaining + " seconds";
        }
        else
        {
            countdownToEnergyValue.text = "Energy at maximum!";
        }

        if(energy > energyMax)
        {
            energy = energyMax;
        }
    }

    public void DecrementEnergy()
    {
        energy -= 3;
        if(energy < 3)
        {
            GameAnalytics.NotEnoughEnergy(energy);
        }
        SaveData();
        remainingEnergyValue.text = energy.ToString();
    }

    public void Play()
    {
        if (energy-3 >= 0)
        {
            DecrementEnergy();
            SaveData();
            SceneManager.LoadScene(Scenes.Load);
        }
        else
        {
            //display no turns message
            //offer rewarded ad opportunity only once
        }
    }

    private void SaveData(bool energyIncrease = false)
    {
        PlayerPrefs.SetInt(SaveManager.remainingEnergyCount, energy);
        if (energyIncrease)
        {
            PlayerPrefs.SetString(SaveManager.previousEnergyUpdateTime, Utilities.UnixTimeNow().ToString());
        }
        PlayerPrefs.Save();
    }

    private void LoadData()
    {
        energy = PlayerPrefs.GetInt(SaveManager.remainingEnergyCount);
        timeLastSaved = (long)Convert.ToDouble(PlayerPrefs.GetString(SaveManager.previousEnergyUpdateTime));
    }

    public void showAdvert()
    {
        if (energy == energyMax)
        {
            GameObject popupPrefab = Resources.Load("Prefabs/UI/MaxEnergyPopup") as GameObject;
            GameObject.Instantiate(popupPrefab, new Vector3(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2, 0), Quaternion.identity);
        }
        else
        {
            GameObject popupPrefab = Resources.Load("Prefabs/UI/WatchAdvertPopup") as GameObject;
            GameObject.Instantiate(popupPrefab, new Vector3(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2, 0), Quaternion.identity);
        }
    }

    void OnApplicationQuit()
    {
        GameAnalytics.QuitGame(startEnergy - energy, Time.time, PlayerPrefs.GetInt(SaveManager.dailyHighScore));
    }
}
