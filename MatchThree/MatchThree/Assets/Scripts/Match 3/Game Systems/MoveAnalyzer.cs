﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MoveAnalyzer// : object
{
    public static void analyze(LinkedList<Tile> explodableGems, Tile swapTile)
    {
        if (explodableGems != null)
        {
            if (!Rules.comboGemsOn)
                return;

            foreach (Tile currentTile in explodableGems)
            {
                if (currentTile.tag == Tags.comboGem)
                {
                    return;
                }
            }

            //check Object is a match 4
            if (isMatchFive(explodableGems))
            {
                spawnComboGem(explodableGems, 6, swapTile);
            }
            else if (isMatchFour(explodableGems))
            {
                //get direction
                Utilities.Direction swipeDirection = getSwipeDirection();

                switch (swipeDirection)
                {
                    case Utilities.Direction.Up:
                        spawnComboGem(explodableGems, 5, swapTile);
                        break;
                    case Utilities.Direction.Right:
                        spawnComboGem(explodableGems, 4, swapTile);
                        break;
                    case Utilities.Direction.Down:
                        spawnComboGem(explodableGems, 5, swapTile);
                        break;
                    case Utilities.Direction.Left:
                        spawnComboGem(explodableGems, 4, swapTile);
                        break;
                    case Utilities.Direction.None:
                        spawnComboGem(explodableGems, 4, swapTile);
                        break;
                    default:
                        spawnComboGem(explodableGems, 4, swapTile);
                        break;
                }
            }
        }
    }

    static Utilities.Direction getSwipeDirection()
    {
        GameManager gameManager = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<GameManager>();

        return gameManager.previousSwipeDirection;
    }

    static void spawnComboGem(LinkedList<Tile> explodableGems, int comboGemID, Tile swapTile)
    {
        Level currentLevel = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();
        SpawnManager spawnManager = currentLevel.spawnManager;

        //Spawn Settings for combo Gem
        Queue<Vector2> possibleSpawnLocations = getPossibleSpawnLocations(explodableGems);
        Vector2 possibleLocation = possibleSpawnLocations.Dequeue();
        Tile upgradableGem = currentLevel.getTile(possibleLocation);

        while (isGemExploding(explodableGems, possibleLocation) || upgradableGem.gameObject.tag == Tags.comboGem 
            || upgradableGem.id == -1 || isBlockingGem(upgradableGem.id) || upgradableGem.isAnimating || upgradableGem.getAnimationCount() > 0
            || upgradableGem == swapTile)
        {
            if (possibleSpawnLocations.Count == 0)
                return;

            possibleLocation = possibleSpawnLocations.Dequeue();
            upgradableGem = currentLevel.getTile(possibleLocation);
        }

        //spawn the combo gem
        spawnManager.createComboGem(upgradableGem, comboGemID);
    }

    static bool isMatchFour(LinkedList<Tile> explodableGems)
    {
        foreach(Tile currentGem in explodableGems)
        {
            Tile tileAbove = getTileInDir(Utilities.Direction.Up, currentGem.Position);
            if(tileAbove != null && explodableGems.Contains(tileAbove))
            {
                tileAbove = getTileInDir(Utilities.Direction.Up, tileAbove.Position);
                if (tileAbove != null && explodableGems.Contains(tileAbove))
                {
                    tileAbove = getTileInDir(Utilities.Direction.Up, tileAbove.Position);
                    if (tileAbove != null && explodableGems.Contains(tileAbove))
                    {
                        return true;
                    }
                }
            }
            Tile tileRight = getTileInDir(Utilities.Direction.Right, currentGem.Position);
            if (tileRight != null && explodableGems.Contains(tileRight))
            {
                tileRight = getTileInDir(Utilities.Direction.Right, tileRight.Position);
                if (tileRight != null && explodableGems.Contains(tileRight))
                {
                    tileRight = getTileInDir(Utilities.Direction.Right, tileRight.Position);
                    if (tileRight != null && explodableGems.Contains(tileRight))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    static bool isMatchFive(LinkedList<Tile> explodableGems)
    {
        foreach (Tile currentGem in explodableGems)
        {
            Tile tileAbove = getTileInDir(Utilities.Direction.Up, currentGem.Position);
            if (tileAbove != null && explodableGems.Contains(tileAbove))
            {
                tileAbove = getTileInDir(Utilities.Direction.Up, tileAbove.Position);
                if (tileAbove != null && explodableGems.Contains(tileAbove))
                {
                    tileAbove = getTileInDir(Utilities.Direction.Up, tileAbove.Position);
                    if (tileAbove != null && explodableGems.Contains(tileAbove))
                    {
                        tileAbove = getTileInDir(Utilities.Direction.Up, tileAbove.Position);
                        if (tileAbove != null && explodableGems.Contains(tileAbove))
                        {
                            return true;
                        }
                    }
                }
            }
            Tile tileRight = getTileInDir(Utilities.Direction.Right, currentGem.Position);
            if (tileRight != null && explodableGems.Contains(tileRight))
            {
                tileRight = getTileInDir(Utilities.Direction.Right, tileRight.Position);
                if (tileRight != null && explodableGems.Contains(tileRight))
                {
                    tileRight = getTileInDir(Utilities.Direction.Right, tileRight.Position);
                    if (tileRight != null && explodableGems.Contains(tileRight))
                    {
                        tileRight = getTileInDir(Utilities.Direction.Right, tileRight.Position);
                        if (tileRight != null && explodableGems.Contains(tileRight))
                        {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    static Tile getTileInDir(Utilities.Direction dir, Vector2 pos)
    {
        Level currentLevel = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();
        Tile currentTile;
        switch (dir)
        {
            case Utilities.Direction.Up:
                pos.y += 1;
                break;
            case Utilities.Direction.Right:
                pos.x += 1;
                break;
            case Utilities.Direction.Down:
                pos.y -= 1;
                break;
            case Utilities.Direction.Left:
                pos.x -= 1;
                break;
            case Utilities.Direction.None:
                break;
            default:
                break;
        }
        currentTile = currentLevel.getTile(pos, true);
        if (currentTile != null)
            return currentTile;
        else
            return null;
    }

    static Queue<Vector2> getPossibleSpawnLocations(LinkedList<Tile> explodableGems)
    {
        Queue<Vector2> possibleSpawnLocations = new Queue<Vector2>();

        foreach (Tile tile in explodableGems)
        {
            possibleSpawnLocations = addNeighboursToFrontier(possibleSpawnLocations, tile.Position);
        }
        return possibleSpawnLocations;
    }

    public static Queue<Vector2> addNeighboursToFrontier(Queue<Vector2> frontier, Vector2 position)
    {
        Tile[,] map = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().map;
        Vector2 mapSize = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().mapSize;

        if (position.x + 1 < mapSize.x)
        {
            frontier.Enqueue(map[(int)position.x + 1, (int)position.y].Position);
        }
        if (position.y + 1 < mapSize.y)
        {
            frontier.Enqueue(map[(int)position.x, (int)position.y + 1].Position);
        }
        if (position.x - 1 >= 0)
        {
            frontier.Enqueue(map[(int)position.x - 1, (int)position.y].Position);
        }
        if (position.y - 1 >= 0)
        {
            frontier.Enqueue(map[(int)position.x, (int)position.y - 1].Position);
        }
        return frontier;
    }

    static Vector2 checkBounds(Vector2 position)
    {
        Level currentLevel = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();
        Vector2 mapSize = currentLevel.mapSize;

        position.x = Mathf.RoundToInt(position.x);
        position.y = Mathf.RoundToInt(position.y);
        //Debug.Log("Click Pos: " + position.ToString());
        if (position.x >= mapSize.x)
        {
            position.x = mapSize.x * currentLevel.tileSize -1;
        }
        if (position.x < 0)
        {
            position.x = 0;
        }
        if (position.y >= mapSize.y)
        {
            position.y = mapSize.y * currentLevel.tileSize -1;
        }
        if (position.y < 0)
        {
            position.y = 0;
        }
        //Debug.Log("Gem Pos: " + position.ToString());
        return position;
    }

    static bool isGemExploding(LinkedList<Tile> explodableGems, Vector2 position)
    {
        foreach(Tile currentGem in explodableGems)
        {
            if (currentGem.Position.x == position.x && currentGem.Position.y == position.y)
                return true;
        }
        return false;
    }

    static bool isBlockingGem(int id)
    {
        if (id == 41 || id == 42 || id == 43 ||
            id == 51 || id == 52 || id == 53)
            return true;
        return false;
    }
}
