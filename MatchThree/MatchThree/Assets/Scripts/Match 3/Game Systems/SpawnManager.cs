﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnManager : MonoBehaviour
{
    LinkedList<Tile> emptyTiles;
    LinkedList<Tile> blockingTiles;
    Utilities spawnSystem;
    private Level currentLevel;
    private GarbageCollection garbage;
    private TileHolder tileHolder;
    
    bool[] columnsUpdateMap;

    // Use this for initialization
    void Start ()
    {
        emptyTiles = new LinkedList<Tile>();
        blockingTiles = new LinkedList<Tile>();
        spawnSystem = Utilities.getInstance();
	    currentLevel = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();
	    garbage = currentLevel.garbage;
        tileHolder = new TileHolder((int)currentLevel.mapSize.x);

        columnsUpdateMap = new bool[(int)currentLevel.mapSize.y];

        for(int i = 0; i < columnsUpdateMap.Length; i++)
        {
            columnsUpdateMap[i] = true;
        }
    }

    public void initMap(Tile[,] map, Vector2 mapSize)
    {
        int initialOffset = (int)mapSize.y + 2;//+2
        for(int y = 0; y < mapSize.y; y++)
        {
            int currentOffset = initialOffset + (y * ((int)mapSize.y - 3)) + 1;//+1
            for(int x = 0; x < mapSize.x; x++)
            {
                Tile currentTile = map[x, y];
                for(int i = 0; i < currentOffset; i++)
                {
                    currentTile.addAnimationTrigger("MoveDown");
                }
                currentTile.addAnimationTrigger("Bounce");
                currentTile.setPosition(x, y + currentOffset);
                currentTile.setMemoryPosition(x, y);
            }
        }
    }

    public void populateTileLists()
    {
        emptyTiles.Clear();
        blockingTiles.Clear();
        foreach (Transform child in transform)
        {
            if (child.GetComponent<Tile>().id == -1)
            {
                emptyTiles.AddLast(child.GetComponent<Tile>());
            }
            if (Utilities.isBlockingTile(child.GetComponent<Tile>().id))
            {
                blockingTiles.AddLast(child.GetComponent<Tile>());
            }
        }
    }

    public void ManualUpdate()
    {
        tileHolder.UpdateTiles(columnsUpdateMap);
        populateTileLists();

        for (int column = 0; column < currentLevel.mapSize.x; column++) //Check each column for an availible spawn position
        {
            if (currentLevel.map[column, (int)currentLevel.mapSize.y - 1].id == -1 && tileHolder.getColumnTileCount(column) == 0)
            {
                int verticalOffset = 2; //how far above the board to spawn
                int row = (int)currentLevel.mapSize.y - 1;
                Tile newTile;
                while (row >= 0 && currentLevel.map[column, row].id == -1)
                {
                    newTile = spawnSystem.CreateGem(column, (int)currentLevel.mapSize.y + verticalOffset, 10);
                    for (int i = 0; i < verticalOffset; i++)
                    {
                        newTile.addAnimationTrigger("MoveDown"); //add the correct number of animations to drop it to the top of the board
                    }
                    tileHolder.holdTile(column, newTile);
                    verticalOffset++;
                    row--;
                }
            }
        }

        for (int x = 0; x < currentLevel.mapSize.x; x++)
        {
            if (tileHolder.getColumnTileCount(x) > 0)
            {
                if (currentLevel.map[x, (int)currentLevel.mapSize.y - 1].id == -1)
                {
                    if (!tileHolder.peekTile(x).isAnimating && tileHolder.peekTile(x).getAnimationCount() == 0) //if finished animating to the top of the board and there's a space to drop into
                    {

                        Tile gotTile = tileHolder.getTile(x);
                        gotTile.transform.parent = transform;
                        gotTile.setMemoryPosition();
                        currentLevel.swapTiles(gotTile, currentLevel.map[(int)gotTile.Position.x, (int)gotTile.Position.y - 1]);
                        gotTile.addAnimationTrigger("MoveDown");
                        gotTile.AnimationUpdate();
                    }

                    columnsUpdateMap[x] = true;
                }
                else
                {
                    columnsUpdateMap[x] = false;
                }
            }
        }
    }

    public void CheckSpawnList(Tile[,] map, Vector2 mapSize)
    {
        populateTileLists();
        LinkedListNode<Tile> currentNode = emptyTiles.First;
        Tile currentEmptyTile;
        for (int count = 0; count < emptyTiles.Count; count++)
        {
            currentEmptyTile = currentNode.Value;
            Tile swapTile;
            while (currentEmptyTile.Position.y != mapSize.y - 1)
            {
                swapTile = currentLevel.getTile(new Vector2(currentEmptyTile.Position.x, currentEmptyTile.Position.y + 1), true);
                if (swapTile != null)
                {
                    currentLevel.swapTiles(currentEmptyTile, swapTile);
                    if (!Utilities.isInvalidTile(swapTile.id))
                    {
                        swapTile.addAnimationTrigger("MoveDown");
                    }
                }
                else
                {
                    break;
                }
            }
            
            currentNode = currentNode.Next;
        }

        populateTileLists();

        foreach (Tile currentTile in blockingTiles)
        {
            Tile belowTile = currentLevel.getTile(new Vector2(currentTile.Position.x, currentTile.Position.y - 1), true);
            if (belowTile != null)
            {
                if (Utilities.isEmptyTile(belowTile.id))
                {
                    Tile newTile = spawnSystem.CreateGem((int)currentTile.Position.x, (int)currentTile.Position.y, 10);
                    newTile.transform.parent = transform;
                    newTile.Position = new Vector2(currentTile.Position.x, currentTile.Position.y - 1);
                    currentLevel.setTile(newTile);
                    newTile.TriggerMoveDownAnim();
                    garbage.addToGarbage(belowTile);
                }
            }
        }
    }

    /*
    public void addToSpawnList(Tile newTile)
    {
        if(!emptyTiles.Contains(newTile))
            emptyTiles.AddLast(newTile);
    }*/

    public void addToSpawnList(Tile newTile)
    {
        Vector2 mapSize = currentLevel.mapSize;

        if (isValidSpawnPosition(newTile.Position))
        {
            Tile spawnTile = spawnSystem.CreateGem((int)newTile.Position.x, (int)mapSize.y, 10);
            spawnTile.transform.parent = transform;
            spawnTile.isBelowTileEmpty();
        }
    }
    
    public void replaceTile(Tile currentTile, int newTileID)
    {
        Tile newTile = spawnSystem.CreateGem((int)currentTile.Position.x, (int)currentTile.Position.y, newTileID);
        currentTile.transform.parent = null;
        newTile.transform.parent = transform;
        currentLevel = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();

        if (newTileID == -1)
            emptyTiles.AddLast(newTile);

        
        currentLevel.setTile(newTile);
        CheckSpawnList(currentLevel.map,currentLevel.mapSize);
        
    }

    public void createComboGem(Tile upgradableGem, int type)
    {

        Debug.Log("Combo gem creation");
        //Tile upgradableGem = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().getTile(_upgradableGem.position);
        Vector2 spawnLocation = upgradableGem.Position;
        
        Tile newTile = spawnSystem.CreateGem((int)spawnLocation.x, (int)spawnLocation.y, type, 4);
        //TODO: WRITE GETTER AND SETTER
        newTile.id = upgradableGem.id;
        upgradableGem.transform.parent = newTile.gameObject.transform;
        newTile.transform.parent = this.transform;
        newTile.GetComponent<comboGem>().childGem = upgradableGem.gameObject;
        GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().upgradeTile(newTile);
    }

    bool isValidSpawnPosition(Vector2 currentTilePos)
    {
        //get current tile postion and check if a spawn is possible
        //spawn will not be possible if a blocking tile is above the current tile
        Tile[,] map = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().map;
        int mapHeight_Y = (int)GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().mapSize.y;
        int distanceToTopOfMap = mapHeight_Y - (int)currentTilePos.y - 1;

        for(int i = 1; i < distanceToTopOfMap; i++)
        {
            int aboveTileID = map[(int)currentTilePos.x, (int)currentTilePos.y + i].id;
            if (aboveTileID == 41 || aboveTileID == 42 || aboveTileID == 43 )
            {
                return false;
            }
        }

        return true;
    }

    bool isBelowBlockingGem(Vector2 currentTilePos)
    {
        //get current tile postion and check if a blocking Gem is directly above the gem
        Tile currentTile = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().getTile(new Vector2(currentTilePos.x, currentTilePos.y + 1), true);
        if (currentTile == null)
            return false;
        if (currentTile.id == 41 || currentTile.id == 42 || currentTile.id == 43)
        {
                return true;
        }
        return false;
    }

    public bool gemsAnimating()
    {
        for(int i = 0; i < tileHolder.ColumnCount; i++)
        {
            if(tileHolder.getColumnTileCount(i) > 0)
            {
                return true;
            }
        }
        return false;
    }
}
