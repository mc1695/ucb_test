﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Segment
{
    public int size;
    public Func<int> resultMethod;
    public string description;

    public Segment(int size, Func<int> resultMethod, string description)
    {
        this.size = size;
        this.resultMethod = resultMethod;
        this.description = description;
    }

    public void chosen()
    {
        resultMethod.Invoke();
    }
}