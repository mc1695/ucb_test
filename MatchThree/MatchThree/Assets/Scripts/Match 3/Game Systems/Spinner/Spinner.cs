﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Spinner
{
    List<Segment> segments;
    int spinnersize = 0;

    public Spinner(List<Segment> segments)
    {
        this.segments = segments;

        foreach(Segment segment in this.segments)
        {
            spinnersize += segment.size;
        }
    }

    public Segment spin()
    {
        System.Random random = new System.Random();
        int spinValue = random.Next(0, spinnersize);

        int runningTotal = 0;

        Segment chosenSegment = null;

        for(int count = 0; count < segments.Count; count++)
        {
            chosenSegment = segments[count];
            runningTotal += segments[count].size;
            if (runningTotal > spinValue)
            {
                break;
            }
        }

        chosenSegment.chosen();

        return chosenSegment;
    }
}
