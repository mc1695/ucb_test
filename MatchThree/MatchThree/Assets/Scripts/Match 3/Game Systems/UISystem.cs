﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System;

public class UISystem : MonoBehaviour
{
    public Canvas mainCanvas;
    //UI ELEMENTS
    public GameObject movesUI;
    public GameObject timeUI;
    public GameObject scoreUI;
    public GameObject timerWarning;
    public GameObject gameOverScreen;
    public GameObject pauseScreen;
    //UI SYSTEMS
    Text maxMovesText;
    Text movesText;
    Text scoreText;
    [SerializeField]
    GameObject pauseButton;
    //TIMER
    Slider timerSlider;
    float difficultyTimer;
    float difficultyMultiplier;

    private bool timerIncreased = false;

    // Use this for initialization
    void Start()
    {
        timerSlider = timeUI.transform.FindChild("Slider").GetComponent<Slider>();
        difficultyMultiplier = 1;
        scoreText = scoreUI.transform.FindChild("PlayerPoints").GetComponent<Text>();
        movesText = movesUI.transform.FindChild("PlayerMovesText").GetComponent<Text>();
        maxMovesText = movesUI.transform.FindChild("PlayerMaxMovesText").GetComponent<Text>();
        timerWarning = mainCanvas.transform.FindChild("TimeWarning").gameObject;
        timerWarning.SetActive(false);
        gameOverScreen.SetActive(false);

        if (pauseScreen == null)
        {
            pauseScreen = Resources.Load<GameObject>("Prefabs/UI/PauseMenu");
            pauseScreen = GameObject.Instantiate<GameObject>(pauseScreen);
        }

    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimer();
    }

    void UpdateTimer()
    {
        if(timerSlider.value < difficultyMultiplier * 10.0f)
        {
            timerWarning.SetActive(true);
        }
        if(timerSlider.value >= difficultyMultiplier * 10.0f)
        {
            timerWarning.SetActive(false);
        }
        if (timerSlider.value > 0)
        {
            timerSlider.value -= Time.deltaTime * difficultyMultiplier;
        }
        difficultyTimer += Time.deltaTime;
        if (difficultyTimer > 15.0f)
        {
            difficultyTimer = 0;
            difficultyMultiplier += 1;
        }
        updateSliderColourAndAnim();
    }

    private void updateSliderColourAndAnim()
    {
        Image timerImage = timerSlider.transform.FindChild("Fill Area").FindChild("Fill").GetComponent<Image>();
        Image bgSliderImage = timerSlider.transform.FindChild("Background").GetComponent<Image>();

        Color bgStartColour = new Color((float)203 / 255, 1, 1);
        Color bgEndColor = new Color(1, (float)232 / 255, (float)224 / 255);
        
        Color green = new Color((float)86 / 255, (float)227 / 255, (float)159 / 255);
        Color yellow = new Color(1, (float)200 / 255, (float)46 / 255);
        Color red = new Color((float)252 / 255, (float)102 / 255, (float)101 / 255);

        bgSliderImage.color = Color.Lerp(bgEndColor, bgStartColour, timerSlider.value / 100);

        float t;
        if (timerSlider.value >= 50.0f)
        {
            t = (100 - timerSlider.value)/50;
            timerImage.color = Color.Lerp(green, yellow, t);
        }
        else
        {
            t = timerSlider.value/50;
            timerImage.color = Color.Lerp(red, yellow, t);
        }

        if (timerIncreased)
        {
            timerIncreased = false;
            Animator animator = timerSlider.transform.FindChild("sliderAnim").GetComponent<Animator>();
            animator.speed = 1.0f;
            animator.SetTrigger("shine");
        }
    }

    internal void setTimer(float value)
    {
        timerSlider.value = value;
    }

    public bool isWarningActive()
    {
        if(timerSlider.value < difficultyMultiplier * 10.0f)
        {
            return true;
        }
        return false;
    }

    //Create the game canvas depending on the current game rules
    public void SetupCanvas(Level.loseRequirement loseReq)
    {
        switch (loseReq)
        {
            case Level.loseRequirement.timer:
                movesUI.SetActive(false);
                break;
            case Level.loseRequirement.noMovesLeft:
                timeUI.SetActive(false);
                break;
            default:
                break;
        }
    }

    public void IncreaseTimer(int amount)
    {
        timerSlider.value += amount;
        timerIncreased = true;
    }

    public void IncreaseScore(int amount)
    {
        scoreText.text = amount.ToString();
    }

    public void IncrementMoves()
    {
        int currentMoves = movesMade();
        currentMoves++;
        movesText.text = currentMoves.ToString("D3");
    }

    public int movesMade()
    {
        return System.Int32.Parse(movesText.text);
    }

    public void setMaxMoves(int maxMoves)
    {
        maxMovesText.text = "/" + maxMoves.ToString("D3");
    }

    public bool isRemainingTime()
    {
        if (timerSlider.value > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void showGameOverScreen(int points)
    {
        pauseButton.SetActive(false);
        timerSlider.gameObject.SetActive(false);
        gameOverScreen.SetActive(true);
        Text displayText = gameOverScreen.transform.FindChild("panel").FindChild("PointsValue").GetComponent<Text>();
        StartCoroutine(countUpTo(points, displayText));
    }

    public void showHighScoreMessage()
    {
        gameOverScreen.transform.FindChild("HighScoreImage").gameObject.SetActive(true);
    }

    IEnumerator countUpTo(int total, Text displayText)
    {
        displayText.text = 0.ToString();

        int currentValue;
        while (Convert.ToInt32(displayText.text) < total)
        {
            currentValue = Convert.ToInt32(displayText.text);
            currentValue += Mathf.RoundToInt(total / 72.0f);

            if(currentValue > total)
            {
                currentValue = total;
            }

            displayText.text = currentValue.ToString();
            yield return null;
        }
    }

    public void MenuButton()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(Scenes.Menu);
    }

    public void RetryButton()
    {
        int energy = PlayerPrefs.GetInt(SaveManager.remainingEnergyCount);

        if(energy - 3 >= 0)
        {
            Time.timeScale = 1.0f;
            PlayerPrefs.SetInt(SaveManager.remainingEnergyCount, energy-3);
            SceneManager.LoadScene(Scenes.Load);
        }
        else
        {
            SceneManager.LoadScene(Scenes.Menu);
        }
    }

    public void PauseButtonClicked()
    {
        pauseScreen.GetComponentInChildren<PauseMenu>().show();
        Time.timeScale = 0.0f;
    }
}

