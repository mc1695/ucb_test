﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockingTile : Tile
{
    public BlockingTile(int x, int y, int points, int id) : base (x, y)
    {
        //test
        Debug.Log("LOADED NEW Blocking Tile: ID: " + id);

    }

    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	public override void ManualUpdate ()
    {
	
	}

    public override void neighbourDestroyed()
    {
        SpawnManager spawnManager = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().spawnManager;
        Level currentLevel = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();
        health--;
        if(health == 1)
        {
            currentLevel.destroyTile(this);
        }
        else
        {
            id--;
            spawnManager.replaceTile(this, id);
        }

    }

    
}
