﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Gem : Tile
{

    public Queue<int> test;

    public Gem(int x, int y, int points, int id) : base (x, y)
    {
        //test
        Debug.Log("LOADED NEW GEM: ID: "+id);
    }

    // Use this for initialization
    void Awake ()
    {
        base.Init_Tile();
        explosionClip = "Audio/Clips/Explode";
    }
	

    public override bool swap(Tile swapWith, Utilities.Direction dir, bool isAnimRequired = true)
    {
        raycastOffset = Utilities.vectorFromDirection(dir);
        swapWith.raycastOffset = Utilities.vectorFromDirection(Utilities.getOpposite(dir));
        //Debug.Log("SWAPPING: " + dir.ToString());
        //check valid move
        if(isValidMove(swapWith) || swapWith.isValidMove(this))
        {
            if (isAnimRequired)
                swapAnim(dir, swapWith);
            return true;
        }
        else
        {
            if (isAnimRequired)
            {   
                swapAnim(dir, swapWith);
                switch (dir)
                {
                    case Utilities.Direction.Up:
                        swapAnim(Utilities.Direction.Down, swapWith);
                        break;
                    case Utilities.Direction.Right:
                        swapAnim(Utilities.Direction.Left, swapWith);
                        break;
                    case Utilities.Direction.Down:
                        swapAnim(Utilities.Direction.Up, swapWith);
                        break;
                    case Utilities.Direction.Left:
                        swapAnim(Utilities.Direction.Right, swapWith);
                        break;
                    default:
                        break;
                }
                //play invalid swap sound 
                GameObject.FindGameObjectWithTag(Tags.mainCamera).transform.FindChild("MusicPlayer").FindChild("InvalidSwapPlayer")
                    .GetComponent<AudioSource>().Play();

                return false;
            }
            return false;
        }        
    }

    public override bool isValidMove(Tile swapWith)
    {
        //swap positions 
        GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().swapTiles(this, swapWith);

        if (this.isMatchThree(this.id, true))
        {
            LinkedList<Tile> explodableGems = getExplodableGems();
            //
            MoveAnalyzer.analyze(explodableGems, swapWith);
            //
            //removeMatch3(explodableGems);
            return true;
        }
        else
        {
            GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().swapTiles(this, swapWith);
            return false;
        }
    }

    public override void removeMatch3(LinkedList<Tile> explodableGems)
    {
        if (this.isMatchThree(this.id,false))
        {
            //replace all explodable gems with empty tiles
            foreach (Tile gem in explodableGems)
            {
                GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().destroyTile(gem);
            }
        }
    }

    public override LinkedList<Tile> getExplodableGems()
    {
        //frontier containing all neighbours
        //check all neighbours are isMatchThree()
        //if yes, add to explodableGems list and frontier, remove that gem from frontier
        Queue<Tile> frontier = new Queue<Tile>();
        HashSet<Tile> visited = new HashSet<Tile>();
        LinkedList<Tile> explodableGems = new LinkedList<Tile>();

        visited.Add(this);
        explodableGems.AddLast(this);
        frontier = addNeighboursToFrontier(frontier, visited);
        

        int gemId = this.id;
            
        while (frontier.Count != 0)
        {
            Tile currentTile = frontier.Dequeue();
            if (!visited.Contains(currentTile) && gemId == currentTile.id && currentTile.isMatchThreeInMemory(gemId))
            {
                explodableGems.AddLast(currentTile);
                frontier = currentTile.addNeighboursToFrontier(frontier, visited);
            }
            visited.Add(currentTile);
        }

        //Check if special gem has exploded and get all affected gems
        LinkedList<Tile> allExplodableGems = new LinkedList<Tile>();
        LinkedListNode<Tile> currentGem = explodableGems.First;
        int gemCount = explodableGems.Count;


        for (int i = 0; i < gemCount; i++)
        {
            allExplodableGems = currentGem.Value.getSpecialExplodableGems(explodableGems, visited);
            currentGem = currentGem.Next;
            //
            int comboGemCount = allExplodableGems.Count;
            LinkedListNode<Tile> currentTile = allExplodableGems.First;

            for (int j = 0; j < comboGemCount; j++)
            {
                explodableGems.AddLast(currentTile.Value);
                currentTile = currentTile.Next;
            }
        }
        


        /////////////////////////////////////////////////
        if (explodableGems.Count >= 3)
        {            
            return explodableGems;
        }
        else
            return null;
    }

    //checks if current tile is already a match3 on the map in memory
    public override bool isMatchThreeInMemory(int gemId)
    {
        Tile[,] map = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().map;

        //physical distance test to remove new gem spawning issues


        if (isInBounds(Utilities.Direction.Up, 1) && isInBounds(Utilities.Direction.Down, 1))
        {
            //check above and below
            if (map[(int)Position.x, (int)Position.y + 1].id == gemId &&
                map[(int)Position.x, (int)Position.y - 1].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Up;
                return true;
            }
        }
        if (isInBounds(Utilities.Direction.Right, 1) && isInBounds(Utilities.Direction.Left, 1))
        {
            //check right and left
            if (map[(int)Position.x + 1, (int)Position.y].id == gemId &&
                    map[(int)Position.x - 1, (int)Position.y].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Right;
                return true;
            }
        }
        if (isInBounds(Utilities.Direction.Up, 2))
        {
            //Up
            if (map[(int)Position.x, (int)Position.y + 1].id == gemId &&
               map[(int)Position.x, (int)Position.y + 2].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Up;
                return true;
            }
        }
        if (isInBounds(Utilities.Direction.Right, 2))
        {
            //Right
            if (map[(int)Position.x + 1, (int)Position.y].id == gemId &&
               map[(int)Position.x + 2, (int)Position.y].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Right;
                return true;
            }
        }
        if (isInBounds(Utilities.Direction.Down, 2))
        {
            //Down
            if (map[(int)Position.x, (int)Position.y - 1].id == gemId &&
               map[(int)Position.x, (int)Position.y - 2].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Down;
                return true;
            }
        }
        if (isInBounds(Utilities.Direction.Left, 2))
        {
            //Left
            if (map[(int)Position.x - 1, (int)Position.y].id == gemId &&
               map[(int)Position.x - 2, (int)Position.y].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Left;
                return true;
            }
        }
        return false;
    }

    //adds all valid neighbour tiles to the frontier
    public override Queue<Tile> addNeighboursToFrontier(Queue<Tile> frontier, HashSet<Tile> visited)
    {
        Tile[,] map = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().map; 
        Vector2 mapSize = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().mapSize;
        
        if (Position.x + 1 < mapSize.x)
        {
            if (!visited.Contains(map[(int)Position.x + 1, (int)Position.y]))
                frontier.Enqueue(map[(int)Position.x + 1, (int)Position.y]);
        }
        if (Position.y + 1 < mapSize.y)
        {
            if (!visited.Contains(map[(int)Position.x, (int)Position.y + 1]))
                frontier.Enqueue(map[(int)Position.x, (int)Position.y + 1]);
        }
        if(Position.x - 1 >= 0)
        {
            if(!visited.Contains(map[(int)Position.x - 1, (int)Position.y]))
                frontier.Enqueue(map[(int)Position.x - 1, (int)Position.y]);
        }
        if (Position.y - 1 >= 0)
        {
            if (!visited.Contains(map[(int)Position.x, (int)Position.y - 1]))
                frontier.Enqueue(map[(int)Position.x, (int)Position.y - 1]);
        }
        return frontier;
    }

    //checks if current tile is already a match3
    public override bool isMatchThree(int gemId, bool isMoving = false)
    {
        Tile[,] map = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().map;
        bool match = false;
        //physical distance test to remove new gem spawning issues
        
        
        if(isInBounds(Utilities.Direction.Up,1) && isInBounds(Utilities.Direction.Down,1))
        {
            //check above and below
            if (map[(int)Position.x, (int)Position.y + 1].id == gemId &&
                map[(int)Position.x, (int)Position.y - 1].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Up;
                match = matchInPosition(Utilities.Direction.Up, 1, this.id, isMoving);
                if (match)
                {
                    match = matchInPosition(Utilities.Direction.Down, 1, this.id, isMoving);
                }
            }
        }
        if(isInBounds(Utilities.Direction.Right, 1) && isInBounds(Utilities.Direction.Left,1) && !match)
        {
            //check right and left
            if (map[(int)Position.x + 1, (int)Position.y].id == gemId &&
                    map[(int)Position.x - 1, (int)Position.y].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Right;
                match = matchInPosition(Utilities.Direction.Right, 1, this.id, isMoving);
                if (match)
                {
                    match = matchInPosition(Utilities.Direction.Left, 1, this.id, isMoving);
                }
            }
        }
        if(isInBounds(Utilities.Direction.Up,2) && !match)
        {
            //Up
            if (map[(int)Position.x, (int)Position.y + 1].id == gemId &&
               map[(int)Position.x, (int)Position.y + 2].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Up;
                match = matchInPosition(Utilities.Direction.Up, 2, this.id, isMoving);
            }
        }
        if (isInBounds(Utilities.Direction.Right, 2) && !match)
        {
            //Right
            if (map[(int)Position.x + 1, (int)Position.y].id == gemId &&
               map[(int)Position.x + 2, (int)Position.y].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Right;
                match = matchInPosition(Utilities.Direction.Right, 2, this.id, isMoving);
            }
        }
        if (isInBounds(Utilities.Direction.Down, 2) && !match)
        {
            //Down
            if (map[(int)Position.x, (int)Position.y - 1].id == gemId &&
               map[(int)Position.x, (int)Position.y - 2].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Down;
                match = matchInPosition(Utilities.Direction.Down, 2, this.id, isMoving);
            }
        }
        if (isInBounds(Utilities.Direction.Left, 2) && !match)
        {
            //Left
            if (map[(int)Position.x - 1, (int)Position.y].id == gemId &&
               map[(int)Position.x - 2, (int)Position.y].id == gemId)
            {
                possibleMoveDirection = Utilities.Direction.Left;
                match = matchInPosition(Utilities.Direction.Left, 2, this.id, isMoving);
            }
        }

        return match;
    }

    

    public override void dropAboveTiles()
    {
        //get list of tiles above this one
        //itterate through
        //call swapDown anim
        //change all position values
        //spawn new tiles off screen 
        int mapHeight = (int)GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().mapSize.y;
        Tile[,] map = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().map;

        int tilesAbove = getEmptyTileCount(Utilities.Direction.Up, this);
        Utilities gemSpawner = Utilities.getInstance();
        LinkedList<Tile> tilesToDrop = new LinkedList<Tile>();

        //get current above tiles
        for (int i = 0; i < tilesAbove; i++)
        {
            Tile currentGem = map[(int)Position.x, (int)Position.y + 1 + i];
            if(currentGem.id >= 0)
                tilesToDrop.AddLast(currentGem);
        }
        //create new tiles and add to list
        //remove the empty tile for each new tile created!!!!!!
        for (int i = 0; i < tilesAbove; i++)
        {
            Tile newGem = gemSpawner.CreateGem((int)Position.x, mapHeight, 10);
            tilesToDrop.AddLast(newGem);
        }

        
        //update currentLevel with positions
        foreach (Tile currentGem in tilesToDrop)
        {
            int dropCount = getEmptyTileCount(Utilities.Direction.Down, currentGem);
            currentGem.Position = new Vector2(currentGem.Position.x, currentGem.Position.y - dropCount);
            

        }

        foreach(Tile currentGem in tilesToDrop)
        {
            GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().setTile(currentGem);
            //drop tile anim
            int dropCount = getEmptyTileCount(Utilities.Direction.Down, currentGem);
            for (int i = 0; i < dropCount; i++)
            {
                GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().getTile(currentGem.Position).addAnimationTrigger("MoveDown");
            }
        }
    }

    int getEmptyTileCount(Utilities.Direction dir, Tile currentGem)
    {
        Tile[,] map = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().map;
        int mapHeight = (int)GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().mapSize.y;
        int emptyTiles = 0;
        if (dir == Utilities.Direction.Down)
        {
            for (int i = (int)currentGem.Position.y - 1; i >= 0; i--)
            {
                if (i >= 0 && i < mapHeight)
                {
                    if (map[(int)currentGem.Position.x, i].id == -1)
                    {
                        emptyTiles++;
                    }
                }
            }
        }
        else if(dir == Utilities.Direction.Up)
        {
            emptyTiles += 1;
            for (int i = (int)currentGem.Position.y + 1; i < mapHeight ; i++)
            {
                if (i >= 0 && i <= mapHeight)
                {
                    if (map[(int)currentGem.Position.x, i].id == 41 || map[(int)currentGem.Position.x, i].id == 42 ||
                        map[(int)currentGem.Position.x, i].id == 43)
                    {
                        return 0;
                    }
                    if (map[(int)currentGem.Position.x, i].id == -1)
                    {
                        emptyTiles++;
                    }
                }
            }
        }
        return emptyTiles;
    }

    public override bool isBelowTileEmpty(bool directly = true)
    {
        if (directly)
        {
            if (GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().map[(int)Position.x, (int)Position.y - 1].id == -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            Level level = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();
            int y = (int)Position.y;
            bool blank = false;
            while (y >= 0)
            {
                if (level.map[(int)Position.x, y].id == -1)
                {
                    blank = true;
                    break;
                }
                y--;
            }
            return blank;
        }
    }

    private void swapAnim(Utilities.Direction dir, Tile swapWith = null)
    {
        try
        {
            if (this.gameObject != null)
            {
                //remove this check?
                if (!isAnimating)
                {
                    switch (dir)
                    {
                        case Utilities.Direction.Up:
                            this.addAnimationTrigger("MoveUp");
                            swapWith.addAnimationTrigger("MoveDown");
                            break;
                        case Utilities.Direction.Right:
                            this.addAnimationTrigger("MoveRight");
                            swapWith.addAnimationTrigger("MoveLeft");
                            break;
                        case Utilities.Direction.Down:
                            this.addAnimationTrigger("MoveDown");
                            swapWith.addAnimationTrigger("MoveUp");
                            break;
                        case Utilities.Direction.Left:
                            this.addAnimationTrigger("MoveLeft");
                            swapWith.addAnimationTrigger("MoveRight");
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        catch(MissingReferenceException)
        {
            //This is to catch when a script is still running on a destroyed object, the object should be fully destroyed in the next frame
            Utilities.DebugMsg("Script Still Running On Destroyed Object");
            return;
        }
    }

    public override void triggerShine()
    {
        addAnimationTrigger("Shine_0" + id);
    }

    public override void AnimationStart(string anim = "default")
    {
        Animator animator = GetComponent<Animator>();
        float defaultSpeed = 4.0f;
        switch (anim)
        {
            case ("shine"):
            case ("ShuffleShrink"):
            case ("ShuffleGrow"):
                animator.speed = 1.0f;
                break;
            case ("default"):
            default:
                animator.speed = defaultSpeed;
                break;
        }

        isAnimating = true;
    }
    public override void AnimationEnd()
    {
        isAnimating = false;
    }
}
