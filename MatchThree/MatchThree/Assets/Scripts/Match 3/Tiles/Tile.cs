﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

public class Tile : MonoBehaviour
{
    [SerializeField]
    private Vector2 position;
    //public Sprite sprite;
    public Vector2 raycastOffset = Vector2.zero;
    public int id;
    public int health;
    public bool isAnimating;
    private Queue<string> animationQueue;
    public Utilities.Direction possibleMoveDirection;
    public int points;

    protected string explosionClip;
    //
    public Tile(int x, int y)
    {
        Position = new Vector2(x, y);
        isAnimating = false;
    }
	// Use this for initialization
	void Awake ()
    {
        Position = new Vector2(transform.position.x, transform.position.y);
        animationQueue = new Queue<string>();
        explosionClip = null;
    }

    protected void Init_Tile()
    {
        Position = new Vector2(transform.position.x, transform.position.y);
        animationQueue = new Queue<string>();
        explosionClip = null;
    }

    public string getAudioClip()
    {
        return explosionClip;
    }

    // Update is called once per frame
    public virtual void ManualUpdate()
    {
        AnimationUpdate();
    }

    public void AnimationUpdate(float animSpeed = 4.0f)
    {
        try
        {
            if(id == -1)
            {
                return;
            }
            if (animationQueue.Count > 0 && !isAnimating)
            {
                isAnimating = true;
                string trigger = animationQueue.Dequeue();

                if (trigger == "ShuffleShrink")
                {
                    setPositionRelative();
                }
                if(trigger == "Wait")
                {
                    trigger = animationQueue.Dequeue();
                    float waitTime = float.Parse(trigger);
                    StartCoroutine(DelayAnimation(waitTime));
                    return;
                }
                //DEBUG
                GetComponent<Animator>().speed = animSpeed;
                ///
                GetComponent<Animator>().SetTrigger(trigger);
                //GetComponent<Animator>().
                
            }
        }
        catch(MissingReferenceException ex)
        {
            Utilities.DebugMsg("Woops Animating a dead object!" + System.Environment.NewLine + "It's Name is " + name + " and it has an id of " + id);
            GameAnalytics.LogError("Animating object with no Animator", "Missing reference exception in AnimationUpdate() in Tile", ex.StackTrace, SceneManager.GetActiveScene().name, "It's Name is " + name + " and it has an id of " + id);
        }
    }

    IEnumerator DelayAnimation(float time)
    {
        //Debug.Log("STARTED WAITING");
        yield return new WaitForSeconds(time);
        //Debug.Log("DONE WAITING");
        isAnimating = false;
    }

    public void setPosition(int x, int y)
    {
        transform.position = new Vector3(x, y, 0);
    }

    public void setPositionRelative()
    {
        transform.position = new Vector3(Position.x,Position.y,0);
    }

    public void setMemoryPosition(int x = -1, int y = -1)
    {
        if (x == -1 || y == -1)
        {
            Position = new Vector2(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        }
        else
            Position = new Vector2(x, y);
    }

    public virtual bool isMovePossible(Tile[,] map, Vector2 mapSize)
    {
        return Utilities.isPossibleMove(map, mapSize, this);
    }

    public bool checkCurrentMatches(Level currentLevel)
    {
        if (id == -1)
            return false;


        if (isMatchThree(this.id))
        {
            if (possibleMoveDirection == Utilities.Direction.None)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        return false;
    }

    public virtual bool swap(Tile swapGem, Utilities.Direction dir, bool isAnimRequired = true)
    {
        return false;
    }
   

    public virtual bool isMatchThree(int gemId, bool isMoving = false)
    {
        return false;
    }

    //checks if current tile is already a match3 on the map in memory
    public virtual bool isMatchThreeInMemory(int gemId)
    {
        return false;
    }


    public virtual LinkedList<Tile> getExplodableGems()
    {
        return new LinkedList<Tile>();
    }

    public virtual void removeMatch3(LinkedList<Tile> explodableGems)
    {
    }

    public virtual bool isValidMove(Tile swapWith)
    {
        return false;
    }

    public virtual bool isInBounds_2()
    {
        Vector2 mapSize = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().mapSize;

        if (Position.x + 2 > mapSize.x && Position.y + 2 > mapSize.y &&
            Position.x - 2 > 0 && Position.y - 2 > 0)
        {
            return true;
        }
        return false;
    }

    public virtual void dropAboveTiles()
    {
        
    }

    public virtual bool isBelowTileEmpty(bool directly = true)
    {
        return false;
    }

    public virtual Queue<Tile> addNeighboursToFrontier(Queue<Tile> frontier, HashSet<Tile> visited)
    {
        return frontier;
    }

    public bool isInBounds(Utilities.Direction dir, int amount)
    {
        Vector2 mapSize = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>().mapSize;
        //check current position isInbounds
        if (Position.x >= mapSize.x || Position.x < 0 ||
            Position.y >= mapSize.y || Position.y < 0)
        {
            return false;
        }

        switch (dir)
        {
            case Utilities.Direction.Up:
                if (Position.y + amount < mapSize.y)
                    return true;
                break;
            case Utilities.Direction.Right:
                if (Position.x + amount < mapSize.x)
                    return true;
                break;
            case Utilities.Direction.Down:
                if (Position.y - amount >= 0)
                    return true;
                break;
            case Utilities.Direction.Left:
                if (Position.x - amount >= 0)
                    return true;
                break;
            case Utilities.Direction.None:
                if (Position.y + amount < mapSize.y && Position.x + amount < mapSize.x &&
                    Position.y - amount >= 0 && Position.x - amount >= 0)
                    return true;
                break;
            default:
                return false;
        }
        return false;
    }

    public virtual void neighbourDestroyed()
    {

    }

    public virtual LinkedList<Tile> getSpecialExplodableGems(LinkedList<Tile> frontier, HashSet<Tile> visited)
    {
        LinkedList<Tile> explodableGems = new LinkedList<Tile>();
        return explodableGems;
    }

    public virtual void TriggerMoveDownAnim()
    {
        addAnimationTrigger("MoveDown");
    }

    public virtual void AnimationStart(string otherTest)
    {
       
    }
    public virtual void AnimationEnd()
    {
    }

    public bool isApproximatelyAt(Vector2 pos, float threshold)
    {
        if (Mathf.Abs(pos.x - Position.x) < threshold && Mathf.Abs(pos.y - Position.y) < threshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /// <summary>
    /// Shoots a raycast in the given direction "through" a given number of gems checking they match
    /// </summary>
    /// <param name="dir">Direction to raycast</param>
    /// <param name="count">gems to check in that direction</param>
    /// <param name="id">The id to check the gems match</param>
    /// <returns>True if gems match and are in place</returns>
    public bool matchInPosition(Utilities.Direction dir, int count, int id, bool isMoving = false)
    {
        bool matchInPos = false;
        float rayDist = 1.0f;

        RaycastHit2D rayHit;
        Level level = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();
        try
        {
            if (this.gameObject != null && !Utilities.isInvalidTile(id) && transform.parent != level.garbage.transform)
            {
                Collider2D col = this.GetComponent<Collider2D>();
                col.enabled = false;

                Collider2D neighbourCol = null;
                Vector2 currPos = transform.position;
                if (isMoving)
                {
                    //when swapping need to check new position for match, not current
                    currPos += raycastOffset;
                    Tile neighbour = level.getTile(Position + -raycastOffset);
                    if (neighbour.gameObject == null)
                    {
                        matchInPos = false;
                        return matchInPos;
                    }
                    neighbourCol = neighbour.GetComponent<Collider2D>();
                    neighbourCol.enabled = false;
                }
                switch (dir)
                {
                    case Utilities.Direction.Down:
                        rayHit = Physics2D.Raycast(currPos, Vector2.down, rayDist);
                        //Debug.DrawRay(currPos, Vector2.down, Color.black, 0.5f, false);
                        break;
                    case Utilities.Direction.Left:
                        rayHit = Physics2D.Raycast(currPos, Vector2.left, rayDist);
                        //Debug.DrawRay(currPos, Vector2.left, Color.black, 0.5f, false);
                        break;
                    case Utilities.Direction.Right:
                        rayHit = Physics2D.Raycast(currPos, Vector2.right, rayDist);
                        //Debug.DrawRay(currPos, Vector2.right, Color.black, 0.5f, false);
                        break;
                    case Utilities.Direction.Up:
                        rayHit = Physics2D.Raycast(currPos, Vector2.up, rayDist);
                        //Debug.DrawRay(currPos, Vector2.up, Color.black, 0.5f, false);
                        break;
                    default:
                        throw new ArgumentException("Cannot check in given direction", "dir");
                }

                col.enabled = true;
                if (isMoving)
                {
                    neighbourCol.enabled = true;
                }

                if (rayHit.transform == null)
                {
                    return false;
                }

                Tile hitTile = rayHit.transform.GetComponent<Tile>();
                if (hitTile.id == id)
                {
                    matchInPos = true;
                }
                count--;
                if (count > 0 && matchInPos) //if we need to do another 'hop' and what we found WAS a match...
                {
                    level.getTile(this.Position + Utilities.vectorFromDirection(dir)).matchInPosition(dir, count, id);
                }
            }
        }
        catch (MissingReferenceException)
        {
            //This is to catch when a script is still running on a destroyed object, the object should be fully destroyed in the next frame
            Debug.LogWarning("Script Still Running On Destroyed Object");
            return false;
        }
        catch (MissingComponentException)
        {
            //This is to catch when a collider is missing from a gem, likely because its been destroyed
            Debug.LogWarning("Collider Missing From Gem");
            return false;
        } catch (NullReferenceException ex)
        {
            Utilities.DebugMsg("Null ref in matchInPosition: " + ex.Message);
            string extraInfo = "Position: " + this.Position.ToString() + "," + "Pos move dir: " + possibleMoveDirection + ". ";

            for (int x = 0; x < level.mapSize.x; x++)
            {
                for(int y = 0; y < level.mapSize.y; y++)
                {
                    extraInfo += "|" + x + "," + y + ":" + level.map[x,y].id;
                }
            }

            GameAnalytics.LogError("Null ref in Tile.matchInPosition", "Null ref caused at " + ex.TargetSite, ex.StackTrace, SceneManager.GetActiveScene().name);
        }

        return matchInPos;
    }

    public virtual void triggerShine()
    {
        return;
    }

    public Vector2 Position
    {
        get
        {
            return position;
        }
        set
        {
            position = value;
        }
    }

    public virtual int getAnimationCount()
    {
        return animationQueue.Count;
    }

    public virtual void addAnimationTrigger(string trigger)
    {
        animationQueue.Enqueue(trigger);
    }
}
