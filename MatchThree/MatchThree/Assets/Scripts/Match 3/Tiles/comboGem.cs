﻿using UnityEngine;
using System.Collections;

public class comboGem : Gem
{
    public int comboId;
    public GameObject childGem;

    public comboGem(int x, int y, int points, int id, int _comboId) : base(x, y, points, id)
    {
        comboId = _comboId;
    }

    // Use this for initialization
    void Start ()
    {
	
	}
	
}
