﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class comboGem_H : comboGem
{
    public comboGem_H(int x, int y, int points, int id, int _comboId) : base(x, y, points, id, _comboId)
    {

    }

    // Use this for initialization
    void Start()
    {

    }

    

    public override LinkedList<Tile> getSpecialExplodableGems(LinkedList<Tile> frontier, HashSet<Tile> visited)
    {
        Level currentLevel = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();
        LinkedList<Tile> explodableGems = new LinkedList<Tile>();
        int mapWidth = (int)currentLevel.mapSize.x;

        for (int i = 0; i < mapWidth; i++)
        {
            if (i != this.Position.x)
            {
                Tile currentTile = currentLevel.getTile(new Vector2(i, this.Position.y));
                //if (!visited.Contains(currentTile))
                {
                    explodableGems.AddLast(currentTile);
                    //if (Utilities.isComboGem(currentTile))
                    //{
                    //    LinkedList<Tile> innerExplodableGems = new LinkedList<Tile>();
                    //    HashSet<Tile> innerVisited = new HashSet<Tile>();
                    //    Queue<Tile> innerFrontier = new Queue<Tile>();


                    //    innerFrontier = currentTile.addNeighboursToFrontier(innerFrontier, visited);
                    //    foreach (Tile tile in currentTile.getSpecialExplodableGems(frontier, visited))
                    //    {
                    //        explodableGems.AddLast(tile);
                    //    }
                        
                    //}
                }
            }
        }
        return explodableGems;
    }
}
