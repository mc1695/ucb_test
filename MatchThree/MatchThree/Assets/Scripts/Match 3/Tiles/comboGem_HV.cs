﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class comboGem_HV : comboGem
{
    public comboGem_HV(int x, int y, int points, int id, int _comboId) : base(x, y, points, id, _comboId)
    {

    }

    // Use this for initialization
    void Start()
    {

    }

    

    public override LinkedList<Tile> getSpecialExplodableGems(LinkedList<Tile> frontier, HashSet<Tile> visited)
    {
        Level currentLevel = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();
        LinkedList<Tile> explodableGems = new LinkedList<Tile>();
        int mapHeight = (int)currentLevel.mapSize.y;
        int mapWidth = (int)currentLevel.mapSize.x;

        for (int i = 0; i < mapHeight; i++)
        {
            if (i != this.Position.y)
            {
                Tile currentTile = currentLevel.getTile(new Vector2(this.Position.x, i));
                explodableGems.AddLast(currentTile);
            }
        }

        for (int i = 0; i < mapWidth; i++)
        {
            if (i != this.Position.x)
            {
                Tile currentTile = currentLevel.getTile(new Vector2(i, this.Position.y));
                explodableGems.AddLast(currentTile);
            }
        }
        return explodableGems;
    }
}
