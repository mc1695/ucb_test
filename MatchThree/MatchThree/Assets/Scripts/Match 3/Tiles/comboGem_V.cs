﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class comboGem_V : comboGem
{
    public comboGem_V(int x, int y, int points, int id, int _comboId) : base(x, y, points, id, _comboId)
    {

    }

    // Use this for initialization
    void Start()
    {

    }

    

    public override LinkedList<Tile> getSpecialExplodableGems(LinkedList<Tile> frontier, HashSet<Tile> visited)
    {
        Level currentLevel = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<Level>();
        LinkedList<Tile> explodableGems = new LinkedList<Tile>();
        int mapHeight = (int)currentLevel.mapSize.y;

        for (int i = 0; i < mapHeight; i++)
        {
            if (i != this.Position.y)
            {
                Tile currentTile = currentLevel.getTile(new Vector2(this.Position.x, i));
                //if (!visited.Contains(currentTile))
                {
                    explodableGems.AddLast(currentTile);
                }
            }
        }
        return explodableGems;
    }
}
