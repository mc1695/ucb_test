﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System.Collections;

public class AudioManager : MonoBehaviour
{
    public AudioMixerSnapshot calm;
    public AudioMixerSnapshot panic;
    public AudioSource warningSource;
    public AudioSource explosionSource;
    public float bpm = 128;
    public float offset = 0.5f;

    private float transistionIn;
    private float transistionOut;
    private float quaterNote;
    private UISystem uisystem;
    private bool warning;
    private float explosionTimer;
    private string currentExplosionClip;
    private AudioClip explosionClip;
    // Use this for initialization
    void Start ()
    {
        quaterNote = 60 / bpm;
        transistionIn = quaterNote;
        transistionOut = quaterNote * 3;
        explosionTimer = 0;

        uisystem = GameObject.FindGameObjectWithTag(Tags.World).GetComponent<GameManager>().uiSystem;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(uisystem.isWarningActive() && !warning)
        {
            panic.TransitionTo(transistionIn);
            warningSource.Play();
            warning = true;
        }
        if(!uisystem.isWarningActive())
        {
            calm.TransitionTo(transistionOut);
            warning = false;
        }
        if(explosionTimer > 0)
        {
            explosionTimer -= Time.deltaTime;
        }
	}

    public void PlayExplosion(string clip)
    {
        if(explosionTimer <= 0)
        {
            explosionTimer += offset;
            if(clip != currentExplosionClip)
            {
                currentExplosionClip = clip;
                explosionClip = Resources.Load(clip) as AudioClip;
            }
            explosionSource.clip = explosionClip;
            explosionSource.Play();
        }
    }
}
