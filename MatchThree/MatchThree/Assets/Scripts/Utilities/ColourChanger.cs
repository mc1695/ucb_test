﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColourChanger : MonoBehaviour
{
    float t = 0;
    bool hasPulsed;

    //publicly accessible variables
    public Image targetImage;
    public Color Stage1 = new Color(1.0f, 0.0f, 0.0f, 1.0f);
    public Color Stage2 = new Color(1.0f, 0.0f, 0.0f, 0.7f);
    public float fadeDuration = 2.0f;



    // Use this for initialization
    void Awake()
    {
        hasPulsed = false;
    }

    // Update is called once per frame
    void Update()
    {
        //check if image has pulsed 
        if (hasPulsed)
        {
            Pulse(targetImage, Stage1, Stage2, fadeDuration);
        }
        else
        {
            Pulse(targetImage, Stage2, Stage1, fadeDuration);
        }

    }

    //function to steadily change from one colour to another over specified time
    void Pulse(Image image, Color startColor, Color endColor, float duration)
    {
        image.color = Color.Lerp(startColor, endColor, t);
        if (t < 1)
        {
            // increase time factor (t) by time divided by specified duration
            t += Time.deltaTime / duration;
        }
        if (t > 1)
        {
            t = 0;
            hasPulsed = !hasPulsed;
        }
    }

    public void initialiseColourChanger(Image newImage, Color color1, Color color2, float fadeTime)
    {
        targetImage = newImage;
        Stage1 = color1;
        Stage2 = color2;
        fadeDuration = fadeTime;
    }


}