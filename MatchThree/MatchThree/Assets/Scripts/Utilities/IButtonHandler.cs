﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IButtonHandler
{
    void leftClicked(Popup popup);
    void rightClicked(Popup popup);
    void middleClicked(Popup popup);
}

