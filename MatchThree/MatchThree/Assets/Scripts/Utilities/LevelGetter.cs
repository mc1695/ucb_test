﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

class LevelGetter
{
    private string baseURL;
    private string user;
    private string pass;
    private string fileNamesKey = "levelNames";

    public LevelGetter(string username, string password, string baseURL = "https://api.bitbucket.org/1.0/repositories/gengame/match3levels/raw/master/")
    {
        user = username;
        pass = password;
        this.baseURL = baseURL;
    }

    /// <summary>
    /// Grab the most up to date levels from te repo
    /// </summary>
    public void updateLocalFiles()
    {
        if (checkForInternetConnection())
        {
            string[] fileNames = getLevelNames();

            foreach (string file in fileNames)
            {
                string contents = getLevelByName(file);
                PlayerPrefs.SetString(file, contents);
            }
        }
        else
        {
            throw new WebException("No internet Connection");
        }
    }

    public void checkForNewFiles()
    {
        if(checkForInternetConnection())
        {
            string reqURL = baseURL + "updatedOn.txt";
            int mostRecentVersion = 0;
            int localVersion = 0;
            try
            {
                string mostRecentVersionString = getResponseAsString(reqURL);
                Debug.Log(mostRecentVersionString.ToString());
                if(mostRecentVersionString == "" || mostRecentVersionString == null)
                {
                    return;
                }
                mostRecentVersion = Int32.Parse(mostRecentVersionString);
                localVersion = PlayerPrefs.GetInt(SaveManager.levelFileVersion, 0);
                if (localVersion >= mostRecentVersion)
                {
                    return;
                }
            }
            catch(TimeoutException ex)
            {
                Utilities.DebugMsg("Timed out Getting Level: " + ex.Message);
                GameAnalytics.LogError("Level get timeout", "Timed out whilst checking current level version", ex.StackTrace, SceneManager.GetActiveScene().name, "Local version: " + mostRecentVersion);
            }
            //Levels must be out of date, delete old copies and redownload file list
            string[] currentFilenames = getLevelNames();
            foreach (string levelName in currentFilenames)
            {
                PlayerPrefs.DeleteKey(levelName);
            }
            PlayerPrefs.DeleteKey(fileNamesKey);
            getLevelNames();
            PlayerPrefs.SetInt(SaveManager.levelFileVersion, mostRecentVersion);
        }
        else
        {
            throw new WebException("No Internet Connection");
        }
    }

    /// <summary>
    /// Get all the remote level names and split them by return character '\n'
    /// </summary>
    /// <returns>All the level names</returns>
    private string[] getLevelNames()
    {
        string[] fileNames;
        string rawFileList;

        rawFileList = PlayerPrefs.GetString(fileNamesKey);
        if (rawFileList != "")
        {
            fileNames = parseFileList(rawFileList);
        }
        else
        {
            if (checkForInternetConnection())
            {
                rawFileList = getResponseAsString(baseURL);
                PlayerPrefs.SetString(fileNamesKey, rawFileList);
                fileNames = parseFileList(rawFileList);
            }
            else
            {
                return null;
            }
        }
        
        return fileNames;
    }

    /// <summary>
    /// Take the raw file list from bitbucket and returns the split file lists
    /// </summary>
    /// <param name="rawFileList"></param>
    /// <returns>The split list of file names</returns>
    private string[] parseFileList(string rawFileList)
    {
        return rawFileList.Split(new string[] { "\n" }, StringSplitOptions.None);
    }

    /// <summary>
    /// creates a single string from a list of strings
    /// Seperates strings with "\n"
    /// </summary>
    /// <param name="filenames"></param>
    /// <returns></returns>
    private string parseToString(string[] filenames)
    {
        string filenamesString = "";

        foreach (string filename in filenames)
        {
            filenamesString = String.Concat(filenamesString,filename+"\n");
        }

        return filenamesString;
    }

    /// <summary>
    /// Retrives a string from a web request to a given url
    /// </summary>
    /// <param name="name">The name of the level file to retreive</param>
    /// <returns>The level data as string</returns>
    private string getResponseAsString(string url)
    {
        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
        request.Credentials = new NetworkCredential(user, pass);

        ServicePointManager.ServerCertificateValidationCallback = myRemoteCertificateValidationCallback;
        request.Timeout = 15000;
        using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
        {
            var reader = new StreamReader(response.GetResponseStream());
            string fileContents = reader.ReadToEnd();
            Utilities.DebugMsg("INITIAL FILE CONTENTS: " + fileContents);
            return fileContents;
        }
    }

    /// <summary>
    /// Get the level data from a given level's name. If the level si not stored locally, the level is retrieved from the repo
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public string getLevelByName(string name)
    {
        string level = PlayerPrefs.GetString(name, "");
        //if no saved level, get it from the internet
        if (level == "")
        {
            if (checkForInternetConnection(true))
            {
                string reqURL = baseURL + name;
                Utilities.DebugMsg("GETTING FILE FROM: " + reqURL);
                level = getResponseAsString(reqURL);
                PlayerPrefs.SetString(name, level);
                addFileToDownloadsList(name);
                GameObject.FindGameObjectWithTag(Tags.World).GetComponent<GameManager>().DownloadedLevel = true;
            }
            else
            {
                Utilities.DebugMsg("RETURNING LEVEL FILE NULL: " + level);
                GameAnalytics.LevelLoadFailed();
                return "";
            }
        }
        Utilities.DebugMsg("RETURNING LEVEL FILE: " + level);
        return level;
    }

    /// <summary>
    /// Returns a random level's data. If no levels are stored locally, will fetch from the repo
    /// </summary>
    /// <returns>Level data</returns>
    public string getLevelRandom()
    {
        string[] files = getLevelNames();
        System.Random rand = new System.Random();
        string fileName = "updatedOn.txt";
        while (fileName == "updatedOn.txt")
        {
            fileName = files[rand.Next(0, files.Length)];

        }
        Utilities.DebugMsg("Level: " + fileName);
        string level = getLevelByName(fileName);
        if(level == "" || level == null)
        {
            Utilities.DebugMsg("BEING THICK, STILL CAN'T FIND FILES");
            //randomly select a downloaded level 
            string[] downloadedLevels = getDownloadedLevelNames();
            fileName = "";
            Utilities.DebugMsg("GENERATING RANDOM FILENAME");
            fileName = downloadedLevels[rand.Next(0, downloadedLevels.Length)];
            Utilities.DebugMsg("SEARCHING FOR: " + fileName);
            if (fileName == "")
            {
                Utilities.DebugMsg("NO LOCAL LEVELS EXIST");
                
                //no local files exist, tell user to connect to internet
                return "";
            }
            level = getLevelByName(fileName);
        }
        return level;
    }

    /// <summary>
    /// Correctly Certifies X509 Security Certificates
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="certificate"></param>
    /// <param name="chain"></param>
    /// <param name="sslPolicyErrors"></param>
    /// <returns>True if certificate is valid, false otherwise</returns>
    private bool myRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        bool isOk = true;
        // If there are errors in the certificate chain, look at each error to determine the cause.
        if (sslPolicyErrors != SslPolicyErrors.None)
        {
            for (int i = 0; i < chain.ChainStatus.Length; i++)
            {
                if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                    }
                }
            }
        }
        return isOk;
    }

    /// <summary>
    /// Checks if there is an internet connection
    /// </summary>
    /// <returns>True if can connect to the internet, false otherwise</returns>
    private static bool checkForInternetConnection(bool isFake = false)
    {
        //Allows seconds checks to be skipped in certain cases
        if (isFake)
        {
            return true;
        }
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.example.com");
            
            request.Timeout = 15000;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            {
                response.GetResponseStream();
                //Utilities.DebugMsg("STATUS CODE: " + response.StatusCode);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        catch(Exception ex)
        {
            Utilities.DebugMsg("REQUEST GET RESPONSE FAILED: "+ex.Message);
            return false;
        }
    }

    /// <summary>
    /// Gets all downloaded levels filenames
    /// </summary>
    public string[] getDownloadedLevelNames()
    {
        Utilities.DebugMsg("BEGIN DESERIALIZE");
        List<string> levelNames = JsonUtility.FromJson<List<string>>(PlayerPrefs.GetString(SaveManager.downloadedLevelNames));
        Utilities.DebugMsg("END DESERIALIZE: " + levelNames.Count);
        if(levelNames == null)
        {
            return new string[1];
        }
        string[] levelNamesString = new string[levelNames.Count];
        int index = 0;
        foreach (string level in levelNames)
        {
            levelNamesString[index] = level;
        }
        Utilities.DebugMsg("FORMATTED STRING: " + levelNamesString);
        
        return levelNamesString;
    }

    public void addFileToDownloadsList(string filename)
    {
        List<string> levelNames = JsonUtility.FromJson<List<string>>(PlayerPrefs.GetString(SaveManager.downloadedLevelNames));
        if(levelNames == null)
        {
            levelNames = new List<string>();
        }
        levelNames.Add(filename);
        PlayerPrefs.SetString(SaveManager.downloadedLevelNames, JsonUtility.ToJson(levelNames));
    }
    
    /// <summary>
    /// Removes all the local level data stored in player prefs including the file list
    /// </summary>
    public void deleteAllLocalLevelData()
    {
        string[] levelNames = getLevelNames();
        if (levelNames != null)
        {
            foreach (string name in levelNames)
            {
                PlayerPrefs.DeleteKey(name);
            }
        }
        PlayerPrefs.DeleteKey(fileNamesKey);
    }
    
}
    
