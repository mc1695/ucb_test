﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class HelpPopup : Popup, IShowHider
{
    Animator parentAnimator;
    Animator selfAnimator;

    [SerializeField]
    private ManualAnimation exampleGameplay;

    private void Awake()
    {
        ParentAnimator = transform.parent.GetComponent<Animator>();
        SelfAnimator = GetComponent<Animator>();
        base.OnAwake();
        exampleGameplay.Paused = true;
    }

    public override void middleClicked()
    {
        if(handler != null)
        {
            handler.middleClicked(this);
        }
        this.hide();
    }

    public void hide()
    {
        SelfAnimator.SetTrigger("MoveDown");
        ParentAnimator.SetTrigger("FadeOut");
        exampleGameplay.Paused = true;
        Time.timeScale = 1.0f;
    }

    public void show()
    {
        SelfAnimator.SetTrigger("MoveUp");
        ParentAnimator.SetTrigger("FadeIn");
        exampleGameplay.Paused = false;
    }


    public Animator SelfAnimator
    {
        get
        {
            return selfAnimator;
        }

        set
        {
            selfAnimator = value;
        }
    }

    public Animator ParentAnimator
    {
        get
        {
            return parentAnimator;
        }

        set
        {
            parentAnimator = value;
        }
    }
}

