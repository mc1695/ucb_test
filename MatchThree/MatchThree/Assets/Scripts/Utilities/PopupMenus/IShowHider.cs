﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface IShowHider
{
    void show();
    void hide();
    Animator SelfAnimator { get; set; }
    Animator ParentAnimator { get; set; }
}
