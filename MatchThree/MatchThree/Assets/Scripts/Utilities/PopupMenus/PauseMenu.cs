﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : Popup, IShowHider
{
    [SerializeField]
    private Button quitButton = null;
    [SerializeField]
    private Button retryButton = null;
    [SerializeField]
    private Button helpButton = null;

    [SerializeField]
    private Animator panelAnimator;
    [SerializeField]
    private Animator backgroundAnimator;

    void Awake()
    {
        base.OnAwake(); //Do all the text setup etc set in the inspector

        if (quitButton == null || retryButton == null || helpButton == null)
        {
            throw new ArgumentNullException("quitButton|retryButton|helpButton", "Buttons on a pause menu must not be null");
        }

        quitButton.onClick.AddListener(quitButtonClicked);
    }

    /// <summary>
    /// Resume button
    /// </summary>
    public override void middleClicked()
    {
        if(handler != null)
        {
            handler.middleClicked(this);
        }
        Time.timeScale = 1.0f;
        hide();
    }

    public void quitButtonClicked()
    {
        GameAnalytics.EndedGameEarly(Time.timeSinceLevelLoad, GameObject.FindGameObjectWithTag(Tags.World).GetComponent<GameManager>().Points);
        SceneManager.LoadScene(Scenes.Menu);
    }

    public void retyButtonClicked()
    {
        GameAnalytics.EndedGameEarly(Time.timeSinceLevelLoad, GameObject.FindGameObjectWithTag(Tags.World).GetComponent<GameManager>().Points);
        SceneManager.LoadScene(Scenes.Load);
    }

    public void helpButtonClicked()
    {
        hide();
        GameObject.FindObjectOfType<HelpPopup>().show();
    }

    public void hide()
    {
        SelfAnimator.SetTrigger("MoveDown");
        ParentAnimator.SetTrigger("FadeOut");
    }

    public void show()
    {
        SelfAnimator.SetTrigger("MoveUp");
        ParentAnimator.SetTrigger("FadeIn");
    }

    public Animator SelfAnimator
    {
        get
        {
            return panelAnimator;
        }

        set
        {
            panelAnimator = value;
        }
    }

    public Animator ParentAnimator
    {
        get
        {
            return backgroundAnimator;
        }

        set
        {
            backgroundAnimator = value;
        }
    }
}
