﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Popup : MonoBehaviour {
     
    [SerializeField]
    private bool leftButtonActive;
    [SerializeField]
    private string leftButtonText = null;

    [SerializeField]
    private bool middleButtonActive;
    [SerializeField]
    private string middleButtonText = null;

    [SerializeField]
    private bool rightButtonActive;
    [SerializeField]
    private string rightButtonText = null;

    [SerializeField]
    private string titleText = "";
    [SerializeField]
    private string bodyText = "";

    private string key;

    public Button leftButton = null;
    public Button rightButton = null;
    public Button middleButton = null;

    private GameObject leftButtonGameObject;
    private GameObject rightButtonGameObject;
    private GameObject middleButtonGameObject;

    [SerializeField]
    private Text title = null;
    [SerializeField]
    private Text body = null;

    protected IButtonHandler handler;

    public virtual void OnAwake()
    {
        title.text = titleText;
        body.text = bodyText;

        if (leftButton != null)
        {
            leftButtonGameObject = leftButton.gameObject;
            leftButtonGameObject.GetComponentInChildren<Text>().text = leftButtonText;
        }

        if (rightButton != null)
        {
            rightButtonGameObject = rightButton.gameObject;
            rightButtonGameObject.GetComponentInChildren<Text>().text = rightButtonText;
        }

        if (middleButton != null)
        {
            middleButtonGameObject = middleButton.gameObject;
            middleButtonGameObject.GetComponentInChildren<Text>().text = middleButtonText;
        }

        LeftButtonActive = leftButtonActive;
        RightButtonActive = rightButtonActive;
        MiddleButtonActive = middleButtonActive;
    }

    private void Awake()
    {
        OnAwake();
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void registerButtonHandler(IButtonHandler handler, string key)
    {
        this.handler = handler;
        this.key = key;
    }

    public virtual void leftClicked()
    {
        if (handler != null)
        {
            handler.leftClicked(this);
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }
    }

    public virtual void rightClicked()
    {
        if (handler != null)
        {
            handler.rightClicked(this);
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }
    }

    public virtual void middleClicked()
    {
        if (handler != null)
        {
            handler.middleClicked(this);
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }
    }

    public bool LeftButtonActive
    {
        set
        {
            if (leftButtonGameObject != null)
            {
                leftButtonActive = value;
                leftButtonGameObject.SetActive(value);
            }
        }
        get
        {
            return leftButtonActive;
        }
    }

    public bool RightButtonActive
    {
        set
        {
            if (rightButtonGameObject != null)
            {
                rightButtonActive = value;
                rightButtonGameObject.SetActive(value);
            }
        }
        get
        {
            return rightButtonActive;
        }
    }

    public bool MiddleButtonActive
    {
        set
        {
            if (middleButtonGameObject != null)
            {
                middleButtonActive = value;
                middleButtonGameObject.SetActive(value);
            }
        }
        get
        {
            return middleButtonActive;
        }
    }

    public string Key
    {
        get
        {
            return key;
        }
    }
}
