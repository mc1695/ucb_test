﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class WatchAdvertPopup : Popup
{
    void Awake()
    {
        base.OnAwake();
    }

    public override void leftClicked()
    {
        //actually show the advert

        //then give the player a prize...
        List<Segment> segments = new List<Segment>();
        segments.Add(new Segment(33, segmentOneChosen, "Segment one"));
        segments.Add(new Segment(33, segmentTwoChosen, "Segment two"));
        segments.Add(new Segment(33, segmentThreeChosen, "Segment three"));

        Spinner spinner = new Spinner(segments);
        spinner.spin();

        base.leftClicked();
    }

    private int segmentOneChosen()
    {
        Debug.Log("Segment one was chosen");
        return 0;
    }
    private int segmentTwoChosen()
    {
        Debug.Log("Segment two was chosen");
        return 0;
    }
    private int segmentThreeChosen()
    {
        Debug.Log("Segment three was chosen");
        return 0;
    }

}
