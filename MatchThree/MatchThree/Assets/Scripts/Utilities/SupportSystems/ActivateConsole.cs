﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ActivateConsole : MonoBehaviour {

    private int counter = 0;
    [SerializeField]
    private int activationThreshold = 7;
    [SerializeField]
    private GameObject console;

    private bool activatedConsole = false;

	public void ConsoleOn()
    {
        if (console == null)
        {
            throw new ArgumentNullException("Can't have null console if it's going to be activated!");
        }

        counter++;
        if (counter == activationThreshold && !activatedConsole)
        {
            console.SetActive(true);
            activatedConsole = true;
            counter = 0;
            this.GetComponent<Image>().color = new Color((float)74 / 255, (float)196 / 255, (float)137 / 255);
            updateButtonText();
        }
        else
        {
            if (activatedConsole)
            {
                updateButtonText();
                if(counter == activationThreshold)
                {
                    PlayerPrefs.DeleteAll();
                    this.GetComponentInChildren<Text>().text = "Done.";
                    Utilities.DebugMsg("Cleared all local data");
                }
            }
        }
        
    }

    private void updateButtonText()
    {
        this.GetComponentInChildren<Text>().text = "Delete saved data in " + (activationThreshold - counter).ToString() + " clicks";
    }
}
