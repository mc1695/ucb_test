﻿using UnityEngine;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;
//using Fabric.Answers;
//using Fabric.Crashlytics;
using System.Diagnostics;

public class GameAnalytics : object
{
    private static bool devMode = true;
    private static string modeString = "_DEV_";
    private static int userID;

    private static GameAnalytics analytics = new GameAnalytics();

    private GameAnalytics()
    {
    }

    public static GameAnalytics getInstance()
    {
        return analytics;
    }

    public void setUp(AnalyticsMode mode, int userID)
    {
        //setMode(mode);
        //GameAnalytics.userID = userID;
        //Crashlytics.SetUserIdentifier(idToString(userID));
    }

    private void setMode(AnalyticsMode mode)
    {
        switch (mode)
        {
            case (AnalyticsMode.DEV):
                modeString = "_DEV_";
                devMode = true;
                break;
            case (AnalyticsMode.BETA):
                modeString = "_BETA_";
                devMode = false;
                break;
            case (AnalyticsMode.PROD):
                modeString = "_PROD_";
                devMode = false;
                break;
        }
    }

    //Custom Events
    public static void OpenedApp(int energy)
    {
        ////check if logged in
        //string stringID = idToString(userID);
        //bool loginStatus = true; //DEBUG
        //// 
        //Answers.LogCustom(modeString+"App Opened", new Dictionary<string, object>
        //{
        //    {"Login Status", loginStatus.ToString()},
        //    {"UserID", stringID},
        //    {"Energy", energy}
        //});   
    }

    public static void PlayedAGame(int energy, float playTime, int points, int moves, bool levelWasDownloaded, float longestTimeWithoutMove)
    {
        ////check if logged in
        //string stringID = idToString(userID);
        //bool loginStatus = true; //DEBUG
        //// 
        //Answers.LogCustom(modeString + "Played a Game", new Dictionary<string, object>
        //{
        //    {"Login Status", loginStatus.ToString()},
        //    {"UserID",stringID },
        //    {"Energy", energy },
        //    {"Play Time", playTime },
        //    {"Points Scored", points },
        //    {"Swaps Made", moves },
        //    {"Downloaded Level", levelWasDownloaded.ToString() },
        //    {"Longest Time Without Move", longestTimeWithoutMove }
        //});
    }

    public static void LevelLoadFailed()
    {
        ////check if logged in
        //string stringID = idToString(userID);
        //bool loginStatus = true; //DEBUG
        //// 
        //Answers.LogCustom(modeString + "Level Load Failed", new Dictionary<string, object>
        //{
        //    {"Login Status", loginStatus.ToString()},
        //    {"UserID", stringID }
        //});
    }

    public static void NotEnoughEnergy(int energy)
    {
        ////check if logged in
        //string stringID = idToString(userID);
        //bool loginStatus = true; //DEBUG
        //// 
        //Answers.LogCustom(modeString + "Not Enough Energy", new Dictionary<string, object>
        //{
        //    {"Login Status", loginStatus.ToString()},
        //    {"UserID", stringID },
        //    {"Energy", energy }
        //});
    }

    public static void ReturnedToGenGameApp()
    {
        ////check if logged in
        //string stringID = idToString(userID);
        //bool loginStatus = true; //DEBUG
        //// 
        //Answers.LogCustom(modeString + "Returned to GenGame", new Dictionary<string, object>
        //{
        //    {"Login Status", loginStatus.ToString()},
        //    {"UserID", stringID }
        //});
    }

    public static void QuitGame(int energySpent, float totalTime, int dailyHighScore)
    {
        ////check if logged in
        //string stringID = idToString(userID);
        //bool loginStatus = true; //DEBUG
        //// 
        //Answers.LogCustom(modeString + "Returned to GenGame", new Dictionary<string, object>
        //{
        //    {"Login Status", loginStatus.ToString()},
        //    {"UserID", stringID },
        //    {"Energy spent", energySpent },
        //    {"Total time", totalTime },
        //    {"Daily high score", dailyHighScore }
        //});
    }
    
    public static void LogError(string name, string description, string stackTrace, string levelName, string otherInfo = null)
    {
        ////Add any extra info to description...
        //Crashlytics.SetKeyValue("Level", levelName);
        //Crashlytics.SetUserIdentifier(idToString(userID));
        //if(otherInfo != null)
        //{
        //    Crashlytics.SetKeyValue("Info", otherInfo);
        //}
        //Crashlytics.RecordCustomException(name, description, stackTrace);
    }

    public static void EndedGameEarly(float totalTime, int score)
    {
        ////check if logged in
        //string stringID = idToString(userID);
        //bool loginStatus = true; //DEBUG
        //// 
        //Answers.LogCustom(modeString + "Ended Game Early", new Dictionary<string, object>
        //{
        //    {"Login Status", loginStatus.ToString()},
        //    {"UserID", stringID},
        //    {"Time Played", totalTime },
        //    {"Points Scored", score }
        //});
    }

    public static void TimedEventCompleted(string name, float time, string otherInfo = null)
    {
        //bool loginStatus = true;
        //string stringID = idToString(userID);
        ////Add any extra info to description...
        //Crashlytics.SetUserIdentifier(idToString(userID));
        //if (otherInfo != null)
        //{
        //    Crashlytics.SetKeyValue("Info", otherInfo);
        //}
        //Answers.LogCustom(modeString + name + " Time Taken", new Dictionary<string, object>
        //{
        //    {"Login Status", loginStatus.ToString()},
        //    {"UserID", stringID},
        //    {"Time Taken", time }
        //});
    }

    public static void PlayerModelData(float averageTimeBetweenMoves, int invalidMovesMade, int swapsMade)
    {
        ////check if logged in
        //string stringID = idToString(userID);
        //// 

        //float invertedAvgMoveTime = 1 / averageTimeBetweenMoves;
        //float dividedByInvalidMove = invertedAvgMoveTime / invalidMovesMade;
        //float multiplySwapsMade = dividedByInvalidMove * swapsMade;
        ////Add one to prevent negative log output
        //multiplySwapsMade++;

        //float output = Mathf.Log10(multiplySwapsMade);
        ////float output = Mathf.Log10((((1 / averageTimeBetweenMoves) / invalidMovesMade) * swapsMade)+1); //FULL ALGORITHIM 


        //Answers.LogCustom(modeString + "Player Model Data", new Dictionary<string, object>
        //{
        //    {"UserID", stringID},
        //    {"Average Time Between Moves", averageTimeBetweenMoves },
        //    {"Invalid Moves Made", invalidMovesMade },
        //    {"Swaps Made", swapsMade },
        //    {"PM Value", output }
        //});
    }

    private static string idToString(int id)
    {
        string stringID = id.ToString();

        if (devMode)
        {
            stringID = modeString;
        }

        return stringID;
    }
}
