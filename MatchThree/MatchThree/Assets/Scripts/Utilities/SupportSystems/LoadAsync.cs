﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Diagnostics;

public class LoadAsync : MonoBehaviour {
    
    private AsyncOperation async;

    [SerializeField]
    Slider slider;
    [SerializeField]
    Text percentageText;

    private float interpolationValue = 0.0f;
    private bool loadingBarReady = false;
    

    // Use this for initialization
    void Start () {
        try
        {
            Time.timeScale = 1.0f;
            StartCoroutine(loadingBar());
            StartCoroutine(loadGameAsync());
            if (slider == null)
            {
                throw new System.ArgumentNullException("Null slider");
            }
        }
        catch (System.NullReferenceException ex)
        {
            // Get stack trace for the exception with source file information
            StackTrace stackTrace = new StackTrace(ex, true);
            // Get the top stack frame and
            // Get the line number from the stack frame
            int line = stackTrace.GetFrame(0).GetFileLineNumber();

            Utilities.DebugMsg("Async scene load failed: " + ex.Message);
            GameAnalytics.LogError("Async scene load failed", "Caught a null reference exception in LoadAsync.Start()", ex.StackTrace, SceneManager.GetActiveScene().name, "Line: " + line);
        }
    }

    IEnumerator loadGameAsync()
    {
        #if UNITY_EDITOR
                Utilities.DebugMsg("Starting background game loading - DO NOT EXIT PLAY MODE, unity will crash (apparently)");
        #endif

        string firstTime = PlayerPrefs.GetString(SaveManager.firstTime, "true");

        if (firstTime != "false")
        {
            PlayerPrefs.SetString(SaveManager.firstTime, "false");
            async = SceneManager.LoadSceneAsync(Scenes.Tutorial);
        }
        else
        {
            async = SceneManager.LoadSceneAsync(Scenes.Game);
        }

        async.allowSceneActivation = false;
        while (async.progress < 0.9f || !loadingBarReady)
        {
            yield return null;
        }
        updateUI(0.99f);
        Utilities.DebugMsg("Finished Loading Game: " + Time.timeSinceLevelLoad);
        GameAnalytics.TimedEventCompleted("Game Scene Load", Time.timeSinceLevelLoad);

        async.allowSceneActivation = true;
    }

    IEnumerator loadingBar()
    {
        float newValue = 0.0f;

        System.Random random = new System.Random();
        

        float stoppingPoint = (float)random.NextDouble();

        while(slider.value < stoppingPoint)
        {
            newValue = Mathf.Lerp(0.0f, stoppingPoint, interpolationValue);
            interpolationValue += Time.deltaTime * 0.75f;
            updateUI(newValue);
            yield return null;
        }
        yield return new WaitForSeconds(0.15f);
        interpolationValue = 0.0f;
        while (slider.value < 0.99f)
        {
            newValue = Mathf.Lerp(stoppingPoint, 0.99f, interpolationValue);
            interpolationValue += Time.deltaTime * 0.85f;
            updateUI(newValue);
            yield return null;
        }
        loadingBarReady = true;
    }

    private void updateUI(float progress)
    {
        slider.value = progress;
        percentageText.text = Mathf.RoundToInt(slider.value * 100) + "%";
    }
}
