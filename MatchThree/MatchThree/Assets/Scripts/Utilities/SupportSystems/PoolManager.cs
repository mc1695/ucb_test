using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PoolManager
{
    Dictionary<string, Stack<GameObject>> pooledObjects;
    GameObject poolObj = new GameObject("Pool");
    int poolSize;

    /// <summary>
    /// Constructor for a new PoolManager
    /// </summary>
    /// <param name="poolSize">How many copies of each object to fill</param>
    public PoolManager(int poolSize)
    {
        pooledObjects = new Dictionary<string, Stack<GameObject>>();
        this.poolSize = poolSize;
    }

    /// <summary>
    /// Takes a non-instatiated object and pools it ready for use
    /// </summary>
    /// <param name="key">The reference key. Should be the gameobject's prefab location in Resources</param>
    /// <param name="gameObj">The object to store</param>
    public void fillPoolAndDisable(string key, GameObject gameObj)
    {
        Stack<GameObject> stack;

        if (!pooledObjects.TryGetValue(key, out stack))
        {
            stack = new Stack<GameObject>();
            pooledObjects.Add(key, stack);
        }

        GameObject temp;
        for (int i = 0; i < poolSize; i++)
        {
            temp = GameObject.Instantiate(gameObj, Vector3.zero, Quaternion.identity) as GameObject;
            temp.transform.parent = poolObj.transform;
            temp.SetActive(false);
            stack.Push(temp);
        }
    }

    /// <summary>
    /// Diasables a gameobject and adds it to the pool with specified key
    /// </summary>
    /// <param name="key"></param>
    /// <param name="gameObj"></param>
    public void disableAndAddToPool(string key, GameObject gameObj)
    {
        Stack<GameObject> stack;

        if (!pooledObjects.TryGetValue(key, out stack))
        {
            stack = new Stack<GameObject>();
            pooledObjects.Add(key, stack);
        }

        gameObj.transform.parent = poolObj.transform;
        gameObj.SetActive(false);
        stack.Push(gameObj);
    }

    /// <summary>
    /// Releases an instantiated gameobject from the pool
    /// </summary>
    /// <param name="key">Key of object to release</param>
    /// <param name="gameObj">The returned gameobject, null if no pooled items</param>
    public GameObject getFromPoolAndEnable(string key)
    {
        GameObject gameObj;
        Stack<GameObject> stack;
        stack = pooledObjects[key];
        if (stack != null && stack.Count > 0)
        {
            gameObj = stack.Pop();
            gameObj.SetActive(true);
            gameObj.transform.parent = null;
        }
        else
        {
            try
            {
                gameObj = Resources.Load<GameObject>(key);
                gameObj = GameObject.Instantiate<GameObject>(gameObj);
            }
            catch
            {
                Debug.LogWarning("The key for pooled objects should be set to its location in resources if the pool is to size dynamically");
                gameObj = null;
            }
        }

        return gameObj;
    }
}
