﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class PretendConsole : MonoBehaviour
{
    
    private Text msgText;

    private LinkedList<string> messages = new LinkedList<string>();
    private LinkedListNode<string> currentMessage;

    GameObject sliderObject;

    [SerializeField]
    private Text fpsText;
    private float deltaTime = 0.0f;
    private float independentDeltaTime = 0.0f;
    private float timeLastFrame = 0.0f;
    private float frameTextUpdateTimer = 0.0f;
    // Use this for initialization
    void Start ()
    {
        if(msgText == null)
        {
            msgText = this.GetComponent<Text>();
        }
        GameObject.DontDestroyOnLoad(this.transform.parent.gameObject);
        msgText.text = "";
        this.gameObject.SetActive(false);
    }

    void Update()
    {
        if (sliderObject == null)
        {
            Slider slider = GameObject.FindObjectOfType<Slider>();

            if (slider != null)
            {
                slider.interactable = true;
                sliderObject = slider.gameObject;
            }
        }

        independentDeltaTime = Time.realtimeSinceStartup - timeLastFrame;
        timeLastFrame = Time.realtimeSinceStartup;
        
        float msec = independentDeltaTime * 1000.0f;
        float fps = 1.0f / independentDeltaTime;

        frameTextUpdateTimer += independentDeltaTime;

        if (frameTextUpdateTimer > 1.0f)
        {
            fpsText.text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            frameTextUpdateTimer = 0.0f;
        }
    }

    private void UpdateUIText(string msg)
    {
        if (msgText == null)
        {
            msgText = this.GetComponent<Text>();
        }
        msgText.text = msg;
    }

    private void ShowCurrentMessage(string preString = "")
    {
        UpdateUIText(preString + " " + currentMessage.Value);
    }

    public void NextMessage()
    {
        if(currentMessage != null && currentMessage.Next != null)
        {
            currentMessage = currentMessage.Next;
            ShowCurrentMessage();
        }
    }

    public void PreviousMessage()
    {
        if(currentMessage != null && currentMessage.Previous != null)
        {
            currentMessage = currentMessage.Previous;
            ShowCurrentMessage();
        }
    }

    public void LastMessage()
    {
        if(messages.Last != null)
        {
            currentMessage = messages.Last;
            ShowCurrentMessage();
        }
    }

    public void FirstMessage()
    {
        if (messages.First != null)
        {
            currentMessage = messages.First;
            ShowCurrentMessage();
        }
    }

    public void PrintMap()
    {
        Level level = FindObjectOfType<Level>();
        string map = "";
        for(int columns = (int)level.mapSize.x-1; columns >= 0; columns--)
        {
            for(int rows =0; rows< level.mapSize.y; rows++)
            {
                map += level.map[rows, columns].Position;
                map += ", " + level.map[rows, columns].id + " | ";
            }
            map += System.Environment.NewLine;
        }
        UpdateMessage(map);
    }

    /// <summary>
    /// Adds the given message to the list to be displayed on the UI
    /// </summary>
    /// <param name="msg">The message to be added</param>
    /// <returns>Estimated time until message is displayed</returns>
    public void UpdateMessage(string msg)
    {
        messages.AddLast(msg);
        if(currentMessage == null || currentMessage == messages.Last.Previous) //if unset or at the end, automatically move to the next message
        {
            currentMessage = messages.Last;
            ShowCurrentMessage();
        }
        Debug.Log(msg);
    }
}
