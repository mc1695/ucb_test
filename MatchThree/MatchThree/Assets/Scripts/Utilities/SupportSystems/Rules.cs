﻿using UnityEngine;
using System.Collections;

public class Rules : object
{
    public static Level.winRequirement currentWinRequirement;
    public static Level.loseRequirement loseRequirementSelected;
    
    //manage turns
    public static int maxMoves;

    public static bool comboGemsOn;
}
