﻿using UnityEngine;
using System.Collections;
using System;

public class SaveManager : MonoBehaviour
{
    public static string remainingEnergyCount = "remaining_move_count";
    public static string previousEnergyUpdateTime = "last_updated";
    public static string dateAdLastWatched = "date_rewarded_advert_last_watched";
    public static string dailyHighScore = "DailyHighScore";
    public static string localLevelNames = "Local_Level_Filenames";
    public static string downloadedLevelNames = "Downloaded_Level_Filenames";
    public static string levelFileVersion = "Level_File_Version";
    public static string energyCountAtLaunch = "energyCountAtLaunch";
    public static string firstTime = "firstTime";

    public static string pointsToScoreData(int scoreData)
    {
        Utilities.DebugMsg("New high score data: " + scoreData.ToString() + "|" + DateTime.Now.ToString());
        return scoreData.ToString() + "|" + DateTime.Now.ToString();
    }
}
