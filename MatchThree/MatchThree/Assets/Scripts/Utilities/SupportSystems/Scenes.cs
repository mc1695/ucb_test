﻿using UnityEngine;
using System.Collections;

public class Scenes : MonoBehaviour
{
    public static string Menu = "Menu";
    public static string Game = "Game";
    public static string Load = "Loading";
    public static string Tutorial = "Tutorial";
}
