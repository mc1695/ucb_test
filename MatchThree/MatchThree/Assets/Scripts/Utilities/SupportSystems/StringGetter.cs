﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;

public class StringGetter {
    
    /// <summary>
    /// Returns the raw string returned from a given url
    /// </summary>
    /// <param name="requestURL">The url the string will be retrieved from</param>
    /// <returns>String from the given url</returns>
    protected static string getResponseAsString(string requestURL)
    {
        string received = null;
        try
        {
            WebClient client = new WebClient();
            received = client.DownloadString(requestURL);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            received = null;
        }
        Debug.LogWarning("Received: " + received);

        return received;
    }

}
