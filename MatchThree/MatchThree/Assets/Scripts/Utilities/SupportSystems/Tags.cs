﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour
{
    public static string World = "World";
    public static string mainCamera = "MainCamera";
    public static string UI = "UI";
    public static string PointsUI = "PointsUI"; 
    public static string Garbage = "Garbage";
    public static string Spawn = "SpawnSystem";
    public static string comboGem = "ComboGem";
    public static string Console = "Console";
    public static string AudioPlayer = "AudioPlayer";
}
