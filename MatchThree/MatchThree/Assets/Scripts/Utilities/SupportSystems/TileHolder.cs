﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TileHolder
{
    LinkedList<Tile>[] holder; //Array of queues acts as the structure
    int columnCount;

    /// <summary>
    /// Create a new Tile Holder object
    /// </summary>
    /// <param name="columnCount">Number of columns available to queue tiles</param>
    public TileHolder(int columnCount)
    {
        holder = new LinkedList<Tile>[columnCount];
        this.columnCount = columnCount;
        empty(); //used for initialisation of the empty queues
    }

    /// <summary>
    /// Clear all tiles
    /// </summary>
    public void empty()
    {
        for (int i = 0; i < columnCount; i++)
        {
            holder[i] = new LinkedList<Tile>();
        }
    }

    /// <summary>
    /// Add a tile to be held
    /// </summary>
    /// <param name="columnIndex">Column to queue the tile in</param>
    /// <param name="tile">Tile to be held</param>
    public void holdTile(int columnIndex, Tile tile)
    {
        holder[columnIndex].AddLast(tile);
    }

    /// <summary>
    /// Add a tile to be held at the front
    /// </summary>
    /// <param name="columnIndex">Column to queue the tile in</param>
    /// <param name="tile">Tile to be held</param>
    public void holdTileFront(int columnIndex, Tile tile)
    {
        holder[columnIndex].AddFirst(tile);
    }

    /// <summary>
    /// Dequeue a tile from a specified column
    /// </summary>
    /// <param name="columnIndex">The column to dequeue from</param>
    /// <returns></returns>
    public Tile getTile(int columnIndex)
    {
        Tile gotTile = holder[columnIndex].First.Value;
        holder[columnIndex].RemoveFirst();
        gotTile.GetComponent<Animator>().enabled = true;
        return gotTile;
    }

    /// <summary>
    /// Peek a tile from a specific column
    /// </summary>
    /// <param name="columnIndex">The column to peek</param>
    /// <returns></returns>
    public Tile peekTile(int columnIndex)
    {
        if (holder[columnIndex].Count == 0)
        {
            return null;
        }

        return holder[columnIndex].First.Value;   
    }

    /// <summary>
    /// Clear the tiles from a column
    /// </summary>
    /// <param name="columnIndex">Column to be cleared</param>
    public void clearColumn(int columnIndex)
    {
        holder[columnIndex].Clear();
    }

    /// <summary>
    /// Call ManualUpdate() on all tiles
    /// </summary>
    public void UpdateTiles(bool[] specificColumns = null)
    {
        if (specificColumns == null) //update all
        {
            for (int i = 0; i < columnCount; i++)
            {
                UpdateColumn(i);
            }
        }
        else //got a map of which columns to update
        {
            for (int i = 0; i < columnCount; i++)
            {
                if (specificColumns[i]) //if map says true...
                {
                    UpdateColumn(i); //...update this column
                }
                else
                {
                    PauseAnimations(i);
                }
            }
        }
    }

    /// <summary>
    /// Pause all the animations
    /// </summary>
    /// <param name="columnIndex"></param>
    private void PauseAnimations(int columnIndex)
    {
        foreach (Tile tile in holder[columnIndex])
        {
            tile.GetComponent<Animator>().enabled = false;
        }
    }

    /// <summary>
    /// Call the animation update on the given column's tiles
    /// </summary>
    /// <param name="columnIndex">Column index</param>
    private void UpdateColumn(int columnIndex)
    {
        foreach (Tile tile in holder[columnIndex])
        {
            tile.GetComponent<Animator>().enabled = true;
            tile.AnimationUpdate();
        }
    }

    /// <summary>
    /// Returns the number of tiles held in the given column
    /// </summary>
    /// <param name="columnIndex">The column to check</param>
    /// <returns>The number of tiles held</returns>
    public int getColumnTileCount(int columnIndex)
    {
        return holder[columnIndex].Count;
    }
    
    /// <summary>
    /// The number of columns in this tile holder
    /// </summary>
    public int ColumnCount
    {
        get
        {
            return columnCount;
        }
    }
}
