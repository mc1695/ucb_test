﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

public class Utilities
{
    public Vector2 startTouch;
    LineRenderer swipeIndicatorLine;

    Material lineRenderMaterial;
    
    GameObject redGem;
    GameObject blueGem;
    GameObject purpleGem;
    GameObject yellowGem;
    GameObject orangeGem;
    GameObject playButtonGem;
    GameObject blankTile;
    GameObject blockingTile_1;
    GameObject blockingTile_2;
    GameObject blockingTile_3;
    GameObject comboGem_H;
    GameObject comboGem_V;
    GameObject comboGem_HV;
    GameObject blueExplosion;
    GameObject yellowExplosion;
    GameObject orangeExplosion;
    GameObject purpleExplosion;
    GameObject redExplosion;

    SortedList<int, GameObject> explosionList;
    SortedList<int, SortedList<int, GameObject>> tileList;

    private static Utilities self = new Utilities();

    public static Utilities getInstance()
    {
        return self;
    }

    private Utilities()
    {
        startTouch = new Vector2();
        
        redGem = Resources.Load("Prefabs/Gems/redGem") as GameObject;
        blueGem = Resources.Load("Prefabs/Gems/blueGem") as GameObject;
        purpleGem = Resources.Load("Prefabs/Gems/purpleGem") as GameObject;
        yellowGem = Resources.Load("Prefabs/Gems/yellowGem") as GameObject;
        orangeGem = Resources.Load("Prefabs/Gems/orangeGem") as GameObject;
        playButtonGem = Resources.Load("Prefabs/Gems/playButtonGem") as GameObject;
        blankTile = Resources.Load("Prefabs/blankTile") as GameObject;
        blockingTile_1 = Resources.Load("Prefabs/Tiles/blockingTile_1") as GameObject;
        blockingTile_2 = Resources.Load("Prefabs/Tiles/blockingTile_2") as GameObject;
        blockingTile_3 = Resources.Load("Prefabs/Tiles/blockingTile_3") as GameObject;

        comboGem_H = Resources.Load("Prefabs/Gems/comboGem_H") as GameObject;
        comboGem_V = Resources.Load("Prefabs/Gems/comboGem_V") as GameObject;
        comboGem_HV = Resources.Load("Prefabs/Gems/comboGem_HV") as GameObject;
        //Materials
        lineRenderMaterial = Resources.Load("Materials/LineMat") as Material;
        //Explosions
        blueExplosion = Resources.Load("Prefabs/Explosions/blueExplosion") as GameObject;
        yellowExplosion = Resources.Load("Prefabs/Explosions/yellowExplosion") as GameObject;
        orangeExplosion = Resources.Load("Prefabs/Explosions/orangeExplosion") as GameObject;
        purpleExplosion = Resources.Load("Prefabs/Explosions/purpleExplosion") as GameObject;
        redExplosion = Resources.Load("Prefabs/Explosions/redExplosion") as GameObject;

        tileList = new SortedList<int, SortedList<int, GameObject>>(2);

        SortedList<int, GameObject> buildingList = new SortedList<int, GameObject>(9);
        buildingList.Add(-1, blankTile);
        buildingList.Add(0, redGem);
        buildingList.Add(1, blueGem);
        buildingList.Add(2, purpleGem);
        buildingList.Add(3, yellowGem);
        buildingList.Add(4, orangeGem);
        buildingList.Add(5, playButtonGem);
        buildingList.Add(41, blockingTile_1);
        buildingList.Add(42, blockingTile_2);
        buildingList.Add(43, blockingTile_3);

        tileList.Add(1, buildingList);

        buildingList = new SortedList<int, GameObject>(3);
        buildingList.Add(4, comboGem_H);
        buildingList.Add(5, comboGem_V);
        buildingList.Add(6, comboGem_HV);

        tileList.Add(4, buildingList);


        explosionList = new SortedList<int, GameObject>(5);
        explosionList.Add(0, redExplosion);
        explosionList.Add(1, blueExplosion);
        explosionList.Add(2, purpleExplosion);
        explosionList.Add(3, yellowExplosion);
        explosionList.Add(4, orangeExplosion);
    }

    public void InitLineRender(GameManager gameManager)
    {
        swipeIndicatorLine = gameManager.GetComponent<LineRenderer>();
        swipeIndicatorLine.material = lineRenderMaterial;
    }

    /// <summary>
    /// Converts the number code in the level files into human readable values
    /// </summary>
    /// <param name="fileCode"> code from level file </param>
    /// <returns></returns>
    /// 
    public Tile CreateGem(int x, int y, int fileCode, int fileCode_Start = 1)
    {
        if (fileCode == 10)
        {
            fileCode = getRandGem();
        }
        SortedList<int, GameObject> internalList;
        tileList.TryGetValue(fileCode_Start, out internalList);

        GameObject tileObject;
        internalList.TryGetValue(fileCode, out tileObject);

        return ((GameObject)MonoBehaviour.Instantiate(tileObject, new Vector3(x, y, 0), Quaternion.identity)).GetComponent<Tile>();

        #region oldSwitch

        //System.Random rand = new System.Random();

        //switch (fileCode_Start)
        //{
        //    case 1:
        //        switch (fileCode)
        //        {
        //            default:
        //                break;
        //            case -1:
        //                GameObject blankTile0 = (GameObject)MonoBehaviour.Instantiate(blankTile, new Vector3(x, y, 0), Quaternion.identity);
        //                return blankTile0.GetComponent<Tile>();
        //            case 10:
        //                GameObject randomGemType = getRandGem();
        //                GameObject gem10 = (GameObject)MonoBehaviour.Instantiate(randomGemType, new Vector3(x, y, 0), Quaternion.identity);
        //                return gem10.GetComponent<Tile>();
        //            case 0:
        //                //Red Gem            
        //                GameObject gem0 = (GameObject)MonoBehaviour.Instantiate(redGem, new Vector3(x, y, 0), Quaternion.identity);
        //                return gem0.GetComponent<Tile>();
        //            case 1:
        //                //Blue Gem          
        //                GameObject gem1 = (GameObject)MonoBehaviour.Instantiate(blueGem, new Vector3(x, y, 0), Quaternion.identity);
        //                return gem1.GetComponent<Tile>();
        //            case 2:
        //                //Purple Gem           
        //                GameObject gem2 = (GameObject)MonoBehaviour.Instantiate(purpleGem, new Vector3(x, y, 0), Quaternion.identity);
        //                return gem2.GetComponent<Tile>();
        //            case 3:
        //                //Yellow Gem            
        //                GameObject gem3 = (GameObject)MonoBehaviour.Instantiate(yellowGem, new Vector3(x, y, 0), Quaternion.identity);
        //                return gem3.GetComponent<Tile>();
        //            case 4:
        //                //orange Gem            
        //                GameObject gem4 = (GameObject)MonoBehaviour.Instantiate(orangeGem, new Vector3(x, y, 0), Quaternion.identity);
        //                return gem4.GetComponent<Tile>();
        //            case 41:
        //                //Blocking tile, Health = 2
        //                GameObject gem41 = (GameObject)MonoBehaviour.Instantiate(blockingTile_1, new Vector3(x, y, 0), Quaternion.identity);
        //                return gem41.GetComponent<Tile>();
        //            case 42:
        //                //Blocking tile, Health = 2
        //                GameObject gem42 = (GameObject)MonoBehaviour.Instantiate(blockingTile_2, new Vector3(x, y, 0), Quaternion.identity);
        //                return gem42.GetComponent<Tile>();
        //            case 43:
        //                //Blocking tile, Health = 2
        //                GameObject gem43 = (GameObject)MonoBehaviour.Instantiate(blockingTile_3, new Vector3(x, y, 0), Quaternion.identity);
        //                return gem43.GetComponent<Tile>();

        //        }
        //        break;
        //    case 2:
        //        break;
        //    case 3:
        //        break;
        //    case 4:
        //        switch (fileCode)
        //        {
        //            case 1:
        //                break;
        //            case 2:
        //                break;
        //            case 3:
        //                break;
        //            case 4:
        //                GameObject comboGemH = (GameObject)MonoBehaviour.Instantiate(comboGem_H, new Vector3(x, y, 0), Quaternion.identity);
        //                //CreateGem(x, y, specialColour, 1).transform.parent = comboGemH.transform;
        //                return comboGemH.GetComponent<Tile>();
        //            case 5:
        //                GameObject comboGemV = (GameObject)MonoBehaviour.Instantiate(comboGem_V, new Vector3(x, y, 0), Quaternion.identity);
        //                //CreateGem(x, y, specialColour, 1).transform.parent = comboGemV.transform;
        //                return comboGemV.GetComponent<Tile>();
        //            case 6:
        //                GameObject comboGemHV = (GameObject)MonoBehaviour.Instantiate(comboGem_HV, new Vector3(x, y, 0), Quaternion.identity);
        //                //CreateGem(x, y, specialColour, 1).transform.parent = comboGemHV.transform;
        //                return comboGemHV.GetComponent<Tile>();
        //            default:
        //                break;
        //        }
        //        break;
        //    default:
        //        break;

        //}

        #endregion

        //Debug.LogWarning("Failure");
        //Gem emptyGem = new Gem(x, y, 10, 0);
        //return emptyGem;
    }

    public void CreateExplosion(int id, Vector3 position)
    {
        GameObject explosion;
        explosionList.TryGetValue(id, out explosion);
        if (explosion != null)
        {
            explosion = GameObject.Instantiate(explosion, position, Quaternion.identity) as GameObject;
            GameObject.Destroy(explosion, explosion.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        }
    }

    /* 0 = tile [-1 = "no tile"; 0 = "hp = 0"; 1 = "hp 1"...]
	 * 1 = gem [-99 = no gem; 
	 * 			from 0 to 6 color gem; 
     * 			    1 = red gem
	 * 				7 = special explosion good 
	 * 				8 = special explosion bad 
	 * 				9 = neutre = it don't explode when 3 in a row
	 * 			10 = random gem; 
	 * 			20 = ?; 
	 * 			30 = ?; 
	 * 			40 = immune block
	 * 			41 = block hp 1
	 * 			42 = block hp 2
	 * 			43 = block hp 3
	 * 			51 = falling block hp 1
	 * 			52 = falling block hp 2
	 * 			53 = falling block hp 3
	 * 			60 = need a
	 * 			61 = need b
	 * 			62 = need c
	 * 			63 = need d
	 * 			64 = need e
	 * 			70 = key a
	 * 			71 = key b
	 * 			72 = key c
	 * 			73 = key d
	 * 			74 = key e
	 * 2 = special tile:
	 * 		0 = no special
	 * 		1 = start
	 * 		2 = goal
	 * 		3 = path
	 * 		10 = door a
	 * 		11 = door b
	 * 		12 = door c
	 * 		13 = door d
	 * 		14 = door e
	 * 		20 = item a
	 * 		21 = item b
	 * 		22 = item c
	 * 		23 = item d
	 * 		24 = item e
	 * 3 = restraint [0 = no padlock; 1 = padlock hp1...
	 * 				11 = ice hp1...
	 * 4 = special [-200 = token
	 * 				-100 = junk
	 * 				0 = no
	 * 				1= destroy_one
	 * 				2= Switch_gem_teleport
	 * 				3= bomb
	 * 				4= horiz
	 * 				5= vertic
	 * 				6= horiz_and_vertic
	 * 				7= destroy_all_same_color
	 * 
	 * 				8= more_time
	 * 				9= more_moves
	 * 				10= more_hp
	 * 				11= rotate_board_L
	 * 				12= rotate_board_R
	 * 
	 * 				13 = destroy single random gems (meteor shower)
	 * 
	 * 				100 = time bomb
	 * 
	 * 
	 * 5 = number of useful moves of this gem [from 0 = none, to 4 = all directions]
		 	6 = up [n. how many gem explode if this gem go up]
		 	7 = down [n. how many gem explode if this gem go down]
		 	8 = right [n. how many gem explode if this gem go right]
		 	9 = left [n. how many gem explode if this gem go left]
		10 = this thing can fall (0=false;1=true) (2 = explode if reach board bottom border)
		11 = explosion in progress (0=false;1=true)
		12 = this tile generate gems (0= no; 1= yes; 2= yes and it is activated)
		13 = tile already checked (0=false;1=true)
		14 = block_hp
	 */

    int getRandGem()
    {
        int randNumber = UnityEngine.Random.Range(0,5);
        //GameObject randGem;
        //switch (randNumber)
        //{
        //    case 0:
        //        randGem = redGem;
        //        break;
        //    case 1:
        //        randGem = blueGem;
        //        break;
        //    case 2:
        //        randGem = purpleGem;
        //        break;
        //    case 3:
        //        randGem = yellowGem;
        //        break;
        //    case 4:
        //        randGem = orangeGem;
        //        break;
        //    default:
        //        randGem = blueGem;
        //        break;
        //}
        return randNumber;
    }


    /// <summary>
    /// Get Direction of Touch Swipe
    /// </summary>
    /// <returns>0 = Up, 1 = Right, 2 = Down, 3 = Left</returns>
    /// 
    public enum Direction
    {
        Up,
        Right,
        Down,
        Left,
        None
    }

    public static Direction getOpposite(Direction currentDir)
    {
        switch (currentDir)
        {
            case Direction.Up:
                return Direction.Down;
            case Direction.Right:
                return Direction.Left; 
            case Direction.Down:
                return Direction.Up;
            case Direction.Left:
                return Direction.Right;
            case Direction.None:
                return Direction.None;
            default:
                return Direction.None;
        }
    }

    public Direction getSwipeDirection()
    {
        Touch swipeTouch = new Touch();
        if (Input.touchCount > 0 || Input.GetMouseButton(0) || Input.GetMouseButtonUp(0))
        {
            //Debug.Log("Touching");
            if (Input.touchCount > 0)
                swipeTouch = Input.GetTouch(0);


            if (swipeTouch.fingerId == 0 || Input.GetMouseButton(0))
            {
                if (swipeTouch.phase == TouchPhase.Began && Input.touchCount >0)
                {
                    startTouch = swipeTouch.position;
                }
                else if(Input.GetMouseButtonDown(0))
                {
                    startTouch = Input.mousePosition;
                }

                if (swipeTouch.phase == TouchPhase.Ended && Input.touchCount > 0)
                {
                    //Debug.Log("Swiped");
                    Vector2 endTouch = swipeTouch.position;
                    Vector2 swipeDir = endTouch - startTouch;

                    Vector3 startTouchWorldPoint = Camera.main.ScreenToWorldPoint(new Vector3(startTouch.x, startTouch.y, 0));
                    Vector3 mousePosWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    startTouchWorldPoint.z = 5;
                    mousePosWorldPoint.z = 5;

                    float distance = Vector2.Distance(startTouchWorldPoint, mousePosWorldPoint);

                    if (distance < 0.6f)
                    {
                        return Utilities.Direction.None;
                    }

                    return getDirection(swipeDir, swipeDir);
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    //Debug.Log("Swiped");
                    Vector2 endTouch = Input.mousePosition;
                    Vector2 swipeDir = endTouch - startTouch;
                    

                    Vector3 startTouchWorldPoint = Camera.main.ScreenToWorldPoint(new Vector3(startTouch.x, startTouch.y, 0));
                    Vector3 mousePosWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    startTouchWorldPoint.z = 5;
                    mousePosWorldPoint.z = 5;

                    float distance = Vector2.Distance(startTouchWorldPoint, mousePosWorldPoint);

                    if (distance < 0.6f)
                    {
                        return Utilities.Direction.None;
                    }

                    return getDirection(swipeDir, swipeDir);
                }
            }

        }

        return Direction.None;
    }
    
    public void swipeIndication()
    {
        if (startTouch.x != 0 && startTouch.y != 0)
        {
            Vector3 startTouchWorldPoint = Camera.main.ScreenToWorldPoint(new Vector3(startTouch.x, startTouch.y, 0));
            Vector3 mousePosWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            startTouchWorldPoint.z = 5;
            mousePosWorldPoint.z = 5;
            
            if (Vector2.Distance(startTouchWorldPoint, mousePosWorldPoint) < 0.6f)
            {
                swipeIndicatorLine.SetColors(Color.red, Color.red);
            }
            else
            {
                swipeIndicatorLine.SetColors(Color.green, Color.green);
            }

            swipeIndicatorLine.SetVertexCount(2);
            swipeIndicatorLine.SetPosition(0, startTouchWorldPoint);
            swipeIndicatorLine.SetPosition(1, mousePosWorldPoint);
            

            //Debug.Log("DRAWING");
        }
    }

    public int mouseUp()
    {
        startTouch.x = 0;
        startTouch.y = 0;

        swipeIndicatorLine.SetVertexCount(0);

        return -1;
    }

    Direction getDirection(Vector2 swipeDir, Vector2 endDir)
    {
        

        //GameObject.FindGameObjectWithTag(Tags.UI).GetComponent<mousePosTracker>().angle = swipeDir;


        if(Mathf.Abs(swipeDir.x) > Mathf.Abs(swipeDir.y))
        {
            if(Mathf.Sign(swipeDir.x) == 1)
            {
                return Direction.Right;
            }
            else
            {
                return Direction.Left;
            }
        }
        else
        {
            if(Mathf.Sign(swipeDir.y) == 1)
            {
                return Direction.Up;
            }
            else
            {
                return Direction.Down;
            }
        }
        
    }

    public static bool isEmptyTile(int id)
    {
        if (id == -1)
        {
            return true;
        }
        return false;
    }

    public static bool isInvalidTile(int id)
    {
        if (id == 41 || id == 42 || id == 43 ||
            id == 51 || id == 52 || id == 53 || id == -1)
        {
            return true;
        }
        return false;
    }
    
    #region IsMatchX

    public static bool isPossibleMove(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (Utilities.isInvalidTile(tile.id))
            return false;

        //xox
        //xox
        //oxo
        //xox
        //xox
        if (isMatchVerticalUpLeft(map, mapSize, tile))
        {
            return true;
        }
        if (isMatchVerticalUpRight(map, mapSize, tile))
        {
            return true;
        }
        if (isMatchVerticalDownLeft(map, mapSize, tile))
        {
            return true;
        }
        if (isMatchVerticalDownRight(map, mapSize, tile))
        {
            return true;
        }

        //xxoxx
        //ooxoo
        //xxoxx
        if (isMatchHorizontalDownLeft(map, mapSize, tile))
        {
            return true;
        }
        if (isMatchHorizontalDownRight(map, mapSize, tile))
        {
            return true;
        }
        if (isMatchHorizontalUpLeft(map, mapSize, tile))
        {
            return true;
        }
        if (isMatchHorizontalUpRight(map, mapSize, tile))
        {
            return true;
        }

        //oxo
        //xox
        //oxo
        if (isMatchDownLeftRight(map,mapSize,tile))
        {
            return true;
        }
        if (isMatchUpLeftRight(map, mapSize, tile))
        {
            return true;
        }

        if (tile.Position.x >= 3)
        {
            if (isMatchLeft(map, tile))
                return true;
        }
        if (tile.Position.y >= 3)
        {
            if (isMatchDown(map, tile))
                return true;
        }
        if (tile.Position.x <= mapSize.x - 4)
        {
            if (isMatchRight(map, tile))
                return true;
        }
        if (tile.Position.y <= mapSize.y - 4)
        {
            if (isMatchUp(map, tile))
                return true;
        }
        if (isMatchBetween(map, mapSize, tile))
        {
            return true;
        }

        return false;
    }

    public static bool isMatchUp(Tile[,] map, Tile tile)
    {
        if (Utilities.isInvalidTile(map[(int)tile.Position.x, (int)tile.Position.y + 1].id))
            return false;
        if (Utilities.isInvalidTile(map[(int)tile.Position.x, (int)tile.Position.y + 2].id))
            return false;
        if (Utilities.isInvalidTile(map[(int)tile.Position.x, (int)tile.Position.y + 3].id))
            return false;

        int matchingTiles = 0;

        if (map[(int)tile.Position.x, (int)tile.Position.y + 1].id == tile.id)
            matchingTiles++;
        if (map[(int)tile.Position.x, (int)tile.Position.y + 2].id == tile.id)
            matchingTiles++;
        if (map[(int)tile.Position.x, (int)tile.Position.y + 3].id == tile.id)
            matchingTiles++;
        if (matchingTiles == 2)
            return true;
        else
            return false;
    }

    public static bool isMatchRight(Tile[,] map, Tile tile)
    {
        if (Utilities.isInvalidTile(map[(int)tile.Position.x + 1, (int)tile.Position.y].id))
            return false;
        if (Utilities.isInvalidTile(map[(int)tile.Position.x + 2, (int)tile.Position.y].id))
            return false;
        if (Utilities.isInvalidTile(map[(int)tile.Position.x + 3, (int)tile.Position.y].id))
            return false;

        int matchingTiles = 0;
        if (map[(int)tile.Position.x + 1, (int)tile.Position.y].id == tile.id)
            matchingTiles++;
        if (map[(int)tile.Position.x + 2, (int)tile.Position.y].id == tile.id)
            matchingTiles++;
        if (map[(int)tile.Position.x + 3, (int)tile.Position.y].id == tile.id)
            matchingTiles++;
        if (matchingTiles == 2)
            return true;
        else
            return false;
    }

    public static bool isMatchDown(Tile[,] map, Tile tile)
    {
        if (Utilities.isInvalidTile(map[(int)tile.Position.x, (int)tile.Position.y - 1].id))
            return false;
        if (Utilities.isInvalidTile(map[(int)tile.Position.x, (int)tile.Position.y - 2].id))
            return false;
        if (Utilities.isInvalidTile(map[(int)tile.Position.x, (int)tile.Position.y - 3].id))
            return false;

        int matchingTiles = 0;
        if (map[(int)tile.Position.x, (int)tile.Position.y - 1].id == tile.id)
            matchingTiles++;
        if (map[(int)tile.Position.x, (int)tile.Position.y - 2].id == tile.id)
            matchingTiles++;
        if (map[(int)tile.Position.x, (int)tile.Position.y - 3].id == tile.id)
            matchingTiles++;
        if (matchingTiles == 2)
            return true;
        else
            return false;
    }

    public static bool isMatchLeft(Tile[,] map, Tile tile)
    {
        if (Utilities.isInvalidTile(map[(int)tile.Position.x - 1, (int)tile.Position.y].id))
            return false;
        if (Utilities.isInvalidTile(map[(int)tile.Position.x - 2, (int)tile.Position.y].id))
            return false;
        if (Utilities.isInvalidTile(map[(int)tile.Position.x - 3, (int)tile.Position.y].id))
            return false;

        int matchingTiles = 0;
        if (map[(int)tile.Position.x - 1, (int)tile.Position.y].id == tile.id)
            matchingTiles++;
        if (map[(int)tile.Position.x - 2, (int)tile.Position.y].id == tile.id)
            matchingTiles++;
        if (map[(int)tile.Position.x - 3, (int)tile.Position.y].id == tile.id)
            matchingTiles++;
        if (matchingTiles == 2)
            return true;
        else
            return false;
    }

    public static bool isMatchBetween(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        int matchingTiles = 0;

        if (tile.Position.x - 1 > 0) //> -> >= ?
        {
            if (map[(int)tile.Position.x - 1, (int)tile.Position.y].id == tile.id)
                matchingTiles++;
        }

        if (tile.Position.x + 1 < mapSize.x)
        {
            if (map[(int)tile.Position.x + 1, (int)tile.Position.y].id == tile.id)
                matchingTiles++;
        }

        if (tile.Position.y + 1 < mapSize.y)
        {
            if (map[(int)tile.Position.x, (int)tile.Position.y + 1].id == tile.id)
                matchingTiles++;
        }

        if (tile.Position.y - 1 > 0) //> -> >= ?
        {
            if (map[(int)tile.Position.x, (int)tile.Position.y - 1].id == tile.id)
                matchingTiles++;
        }


        if (matchingTiles == 3)
            return true;
        else
            return false;

    }

    public static bool isMatchVerticalUpRight(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (tile.Position.x + 1 >= mapSize.x || tile.Position.y + 2 >= mapSize.y)
        {
            return false;
        }

        if (Utilities.isInvalidTile(map[(int)tile.Position.x + 1, (int)tile.Position.y].id))
        {
            return false;
        }

        Vector2 tileA = new Vector2(tile.Position.x + 1, tile.Position.y + 1);
        Vector2 tileB = new Vector2(tile.Position.x + 1, tile.Position.y + 2);

        if (Utilities.isInvalidTile(map[(int)tileA.x, (int)tileA.y].id))
        {
            return false;
        }
        if (Utilities.isInvalidTile(map[(int)tileB.x, (int)tileB.y].id))
        {
            return false;
        }

        if (map[(int)tileA.x, (int)tileA.y].id == tile.id && map[(int)tileB.x, (int)tileB.y].id == tile.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool isMatchVerticalUpLeft(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (tile.Position.x - 1 < 0 || tile.Position.y + 2 >= mapSize.y)
        {
            return false;
        }

        if (Utilities.isInvalidTile(map[(int)tile.Position.x - 1, (int)tile.Position.y].id))
        {
            return false;
        }

        Vector2 tileA = new Vector2(tile.Position.x - 1, tile.Position.y + 1);
        Vector2 tileB = new Vector2(tile.Position.x - 1, tile.Position.y + 2);

        if (Utilities.isInvalidTile(map[(int)tileA.x, (int)tileA.y].id))
        {
            return false;
        }
        if (Utilities.isInvalidTile(map[(int)tileB.x, (int)tileB.y].id))
        {
            return false;
        }

        if (map[(int)tileA.x, (int)tileA.y].id == tile.id && map[(int)tileB.x, (int)tileB.y].id == tile.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool isMatchVerticalDownRight(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (tile.Position.x + 1 >= mapSize.x || tile.Position.y - 2 < 0)
        {
            return false;
        }

        if (Utilities.isInvalidTile(map[(int)tile.Position.x + 1, (int)tile.Position.y].id))
        {
            return false;
        }

        Vector2 tileA = new Vector2(tile.Position.x + 1, tile.Position.y - 1);
        Vector2 tileB = new Vector2(tile.Position.x + 1, tile.Position.y - 2);

        if (Utilities.isInvalidTile(map[(int)tileA.x, (int)tileA.y].id))
        {
            return false;
        }
        if (Utilities.isInvalidTile(map[(int)tileB.x, (int)tileB.y].id))
        {
            return false;
        }

        if (map[(int)tileA.x, (int)tileA.y].id == tile.id && map[(int)tileB.x, (int)tileB.y].id == tile.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool isMatchVerticalDownLeft(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (tile.Position.x - 1 < 0 || tile.Position.y - 2 < 0)
        {
            return false;
        }

        if (Utilities.isInvalidTile(map[(int)tile.Position.x - 1, (int)tile.Position.y].id))
        {
            return false;
        }

        Vector2 tileA = new Vector2(tile.Position.x - 1, tile.Position.y - 1);
        Vector2 tileB = new Vector2(tile.Position.x - 1, tile.Position.y - 2);

        if (Utilities.isInvalidTile(map[(int)tileA.x, (int)tileA.y].id))
        {
            return false;
        }
        if (Utilities.isInvalidTile(map[(int)tileB.x, (int)tileB.y].id))
        {
            return false;
        }

        if (map[(int)tileA.x, (int)tileA.y].id == tile.id && map[(int)tileB.x, (int)tileB.y].id == tile.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool isMatchHorizontalDownLeft(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (tile.Position.x - 2 < 0 || tile.Position.y - 1 < 0)
        {
            return false;
        }

        if (Utilities.isInvalidTile(map[(int)tile.Position.x, (int)tile.Position.y - 1].id))
        {
            return false;
        }

        Vector2 tileA = new Vector2(tile.Position.x - 1, tile.Position.y - 1);
        Vector2 tileB = new Vector2(tile.Position.x - 2, tile.Position.y - 1);

        if (Utilities.isInvalidTile(map[(int)tileA.x, (int)tileA.y].id))
        {
            return false;
        }
        if (Utilities.isInvalidTile(map[(int)tileB.x, (int)tileB.y].id))
        {
            return false;
        }

        if (map[(int)tileA.x, (int)tileA.y].id == tile.id && map[(int)tileB.x, (int)tileB.y].id == tile.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool isMatchHorizontalDownRight(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (tile.Position.x + 2 >= mapSize.x || tile.Position.y - 1 < 0)
        {
            return false;
        }

        if (Utilities.isInvalidTile(map[(int)tile.Position.x, (int)tile.Position.y - 1].id))
        {
            return false;
        }

        Vector2 tileA = new Vector2(tile.Position.x + 1, tile.Position.y - 1);
        Vector2 tileB = new Vector2(tile.Position.x + 2, tile.Position.y - 1);

        if (Utilities.isInvalidTile(map[(int)tileA.x, (int)tileA.y].id))
        {
            return false;
        }
        if (Utilities.isInvalidTile(map[(int)tileB.x, (int)tileB.y].id))
        {
            return false;
        }

        if (map[(int)tileA.x, (int)tileA.y].id == tile.id && map[(int)tileB.x, (int)tileB.y].id == tile.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool isMatchHorizontalUpLeft(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (tile.Position.x - 2 < 0 || tile.Position.y + 1 >= mapSize.y)
        {
            return false;
        }

        if (Utilities.isInvalidTile(map[(int)tile.Position.x, (int)tile.Position.y + 1].id))
        {
            return false;
        }

        Vector2 tileA = new Vector2(tile.Position.x - 1, tile.Position.y + 1);
        Vector2 tileB = new Vector2(tile.Position.x - 2, tile.Position.y + 1);

        if (Utilities.isInvalidTile(map[(int)tileA.x, (int)tileA.y].id))
        {
            return false;
        }
        if (Utilities.isInvalidTile(map[(int)tileB.x, (int)tileB.y].id))
        {
            return false;
        }

        if (map[(int)tileA.x, (int)tileA.y].id == tile.id && map[(int)tileB.x, (int)tileB.y].id == tile.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool isMatchHorizontalUpRight(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (tile.Position.x + 2 >= mapSize.x || tile.Position.y + 1 >= mapSize.y)
        {
            return false;
        }

        if (Utilities.isInvalidTile(map[(int)tile.Position.x, (int)tile.Position.y + 1].id))
        {
            return false;
        }

        Vector2 tileA = new Vector2(tile.Position.x + 1, tile.Position.y + 1);
        Vector2 tileB = new Vector2(tile.Position.x + 2, tile.Position.y + 1);

        if (Utilities.isInvalidTile(map[(int)tileA.x, (int)tileA.y].id))
        {
            return false;
        }
        if (Utilities.isInvalidTile(map[(int)tileB.x, (int)tileB.y].id))
        {
            return false;
        }

        if (map[(int)tileA.x, (int)tileA.y].id == tile.id && map[(int)tileB.x, (int)tileB.y].id == tile.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool isMatchDownLeftRight(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (tile.Position.x - 1 < 0 || tile.Position.y - 1 < 0 || tile.Position.x + 1 >= mapSize.x)
        {
            return false;
        }

        if (Utilities.isInvalidTile(map[(int)tile.Position.x - 1, (int)tile.Position.y - 1].id) || Utilities.isInvalidTile(map[(int)tile.Position.x + 1, (int)tile.Position.y - 1].id))
        {
            return false;
        }

        Vector2 tileA = new Vector2(tile.Position.x - 1, tile.Position.y - 1);
        Vector2 tileB = new Vector2(tile.Position.x + 1, tile.Position.y - 1);

        if (Utilities.isInvalidTile(map[(int)tileA.x, (int)tileA.y].id))
        {
            return false;
        }
        if (Utilities.isInvalidTile(map[(int)tileB.x, (int)tileB.y].id))
        {
            return false;
        }

        if (map[(int)tileA.x, (int)tileA.y].id == tile.id && map[(int)tileB.x, (int)tileB.y].id == tile.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool isMatchUpLeftRight(Tile[,] map, Vector2 mapSize, Tile tile)
    {
        if (tile.Position.x - 1 < 0 || tile.Position.y + 1 >= mapSize.y || tile.Position.x + 1 >= mapSize.x)
        {
            return false;
        }

        if (Utilities.isInvalidTile(map[(int)tile.Position.x - 1, (int)tile.Position.y + 1].id) || Utilities.isInvalidTile(map[(int)tile.Position.x + 1, (int)tile.Position.y + 1].id))
        {
            return false;
        }

        Vector2 tileA = new Vector2(tile.Position.x - 1, tile.Position.y + 1);
        Vector2 tileB = new Vector2(tile.Position.x + 1, tile.Position.y + 1);

        if (Utilities.isInvalidTile(map[(int)tileA.x, (int)tileA.y].id))
        {
            return false;
        }
        if (Utilities.isInvalidTile(map[(int)tileB.x, (int)tileB.y].id))
        {
            return false;
        }

        if (map[(int)tileA.x, (int)tileA.y].id == tile.id && map[(int)tileB.x, (int)tileB.y].id == tile.id)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion

    public static bool isComboGem(Tile tile)
    {
        if(tile.GetType() == typeof(comboGem))
        {
            return true;
        }else
        {
            return false;
        }
    }
    public static bool isBlockingTile(int id)
    {
        if (id == 41 || id == 42 || id == 43 ||
            id == 51 || id == 52 || id == 53)
        {
            return true;
        }
        return false;
    }

    //WILL MOVE INTO API SYSTEM EVENTUALLY
    public static void launchGenGameApp()
    {

    }

    public static void DebugMsg(string msg, PretendConsole console = null)
    {
        if(console == null)
        {
            GameObject consoleObj = GameObject.FindGameObjectWithTag(Tags.Console);
            if(consoleObj == null)
            {
                return;
            }
            console = consoleObj.GetComponent<PretendConsole>();
        }
        if (console != null)
        {
            console.UpdateMessage(msg);
        }
        else
        {
            //Debug.LogWarning("Tried to send message to PretendConsole but no console attached");
        }
    }

    public static Vector2 vectorFromDirection(Direction dir)
    {
        switch (dir)
        {
            case Direction.Down:
                return Vector2.down;
            case Direction.Left:
                return Vector2.left;
            case Direction.None:
                return Vector2.zero;
            case Direction.Right:
                return Vector2.right;
            case Direction.Up:
                return Vector2.up;
            default:
                throw new NotImplementedException("Direction cannot be converted");
        }
    }

    public static Direction directionFromVector(Vector2 vect)
    {
        switch (vect.ToString())
        {
            case "(0,-1)":
                return Direction.Down;
            case "(-1,0)":
                return Direction.Left;
            case "(0,0)":
                return Direction.None;
            case "(1,0)":
                return Direction.Right;
            case "(0,1)":
                return Direction.Up;
            default:
                throw new NotImplementedException("Direction cannot be converted");
        }
    }

    public static long UnixTimeNow(float reduceByFactor = 1.0f)
    {
        TimeSpan timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
        return (long)(timeSpan.TotalSeconds/reduceByFactor);
    }
}
