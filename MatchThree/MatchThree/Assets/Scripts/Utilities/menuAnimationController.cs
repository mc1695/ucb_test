﻿using UnityEngine;
using System.Collections;

public class menuAnimationController : MonoBehaviour
{
    Utilities utilities;
    Tile[,] tileList;

    int upperBound;
    int lowerBound;

    GameObject title;
    GameObject playText;
    GameObject playButton;
    bool isSwapped = false;
    float animTimer = 0.0f;    
    // Use this for initialization
    void Start ()
    {
        title = GameObject.FindGameObjectWithTag(Tags.UI).transform.FindChild("Title").gameObject;
        title.SetActive(false);
        playText = GameObject.FindGameObjectWithTag(Tags.UI).transform.FindChild("PlayText").gameObject;
        playText.SetActive(false);
        playButton = GameObject.FindGameObjectWithTag(Tags.PointsUI).transform.FindChild("PlayButton").gameObject;
        playButton.SetActive(false);



        tileList = new Tile[3, 4];
        utilities = Utilities.getInstance();

        upperBound = 4;
        lowerBound = 0;
        initTileList();
        staggerTiles();
	}

    /// <summary>
    /// Creates the tiles line by line by randomly generating 
    /// </summary>
    void initTileList()
    {
        //initialise random tile IDs
        
        int row3ID = -1;
        int row2ID = -1;
        row3ID = UnityEngine.Random.Range(lowerBound, upperBound+1);
        while (row2ID == -1 || row2ID == row3ID)
        {
            row2ID = UnityEngine.Random.Range(lowerBound, upperBound + 1);
        }

        //Line 3
        tileList[0, 3] = utilities.CreateGem(0, 3, row3ID);
        tileList[1, 3] = utilities.CreateGem(1, 3, row2ID);
        tileList[2, 3] = utilities.CreateGem(2, 3, row3ID);
        
        //Line 2
        tileList[0, 2] = utilities.CreateGem(0, 2, row2ID);
        tileList[1, 2] = utilities.CreateGem(1, 2, row3ID);
        tileList[2, 2] = utilities.CreateGem(2, 2, row2ID);

        //Line 1
        int currentID = row3ID;

        currentID = decrementID(currentID);
        tileList[0, 1] = utilities.CreateGem(0, 1, currentID);
        tileList[1, 1] = utilities.CreateGem(1, 1, 5);
        currentID = decrementID(currentID);
        tileList[2, 1] = utilities.CreateGem(2, 1, currentID);

        //Line 0
        currentID = decrementID(currentID);
        tileList[0, 0] = utilities.CreateGem(0, 0, currentID);
        currentID = decrementID(currentID);
        tileList[1, 0] = utilities.CreateGem(1, 0, currentID);
        currentID = decrementID(currentID);     
        tileList[2, 0] = utilities.CreateGem(2, 0, currentID);

    }

    int decrementID(int id)
    {
        if(id == 0)
        {
            id = upperBound;
        }
        else
        {
            id--;
        }
        return id;
    }

    void staggerTiles()
    {
        int count = 0;
        int initialOffset = 5;
        for (int y = 0; y < 4; y++)
        {
            for (int x = 2; x >= 0; x--)
            {
                int currentOffset = initialOffset + count;
                Tile currentTile = tileList[x, y];
                for (int i = 0; i < currentOffset; i++)
                {
                    currentTile.addAnimationTrigger("MoveDown");
                }
                currentTile.addAnimationTrigger("Bounce");
                currentTile.setPosition(x, y + currentOffset);
                currentTile.setMemoryPosition(x, y);
                count++;
            }
        }
        Debug.Log("TOTAL: " + count);
    }

    // Update is called once per frame
    void Update ()
    {
        bool isAnimationFinished = true;
        foreach (Tile currentTile in tileList)
        {
            currentTile.AnimationUpdate(2.0f);
            if (currentTile.id == -1)
            {
                continue;
            }
            if(currentTile.isAnimating || currentTile.getAnimationCount() > 0)
            {
                isAnimationFinished = false;
            }
        }
        if (isAnimationFinished)
        {
            animTimer += Time.deltaTime;
            if (animTimer < 1.0f)
            { 
                return;
            }
            if (!isSwapped)
            {
                tileList[1, 2].addAnimationTrigger("MoveUp");
                tileList[1, 3].addAnimationTrigger("MoveDown");
                isSwapped = true;
                return;
            }
            for (int y = 2; y < 4; y++)
            {
                for(int x = 0; x < 3; x++)
                {
                    utilities.CreateExplosion(tileList[x, y].id, tileList[x, y].transform.position);
                    title.SetActive(true);
                    playButton.SetActive(true);
                    playText.SetActive(true);
                    Destroy(tileList[x, y].gameObject);
                }
            }
            Destroy(tileList[1, 1].gameObject);
            //AND NOW COMMIT SUICIDE
            Destroy(this);
        }
	}
}
