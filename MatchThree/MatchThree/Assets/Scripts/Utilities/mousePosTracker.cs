﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class mousePosTracker : MonoBehaviour
{
    public Text trackerText;
    public Text lastAngleText;
    public Text swipeText;
    public Utilities.Direction swipeDir;
    public Vector2 angle;
	// Use this for initialization
	void Start ()
    {
        //this.gameObject.GetComponent<Text>();
        swipeDir = Utilities.Direction.None;
        angle = new Vector2();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        trackerText.text = mousePosition.ToString();
        lastAngleText.text = angle.ToString();
        swipeText.text = swipeDir.ToString();

	}
}
